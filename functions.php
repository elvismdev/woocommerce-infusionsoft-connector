<?php
error_reporting(1);
ini_set("display_errors", "On");
//for test payments use merchant id of 12 and this: http://www.worldpay.com/support/kb/bg/testandgolive/tgl5103.html
if (!class_exists('WC_Payment_Gateway')) {
    return;
}
include_once(plugin_dir_path(__FILE__) . 'isdk.php');

if (isset($_GET['aff'])) {
    $_SESSION['is_aff_id'] = $_GET['aff'];
}else if (isset($_GET['affiliate'])) {
    $_SESSION['is_aff_id'] = $_GET['affiliate'];
} else if (isset($_COOKIE['is_aff_id'])) {
    $_SESSION['is_aff_id'] = $_COOKIE['is_aff_id'];
}
if (isset($_SESSION['is_aff_id'])) {
    setcookie('is_aff_id', $_SESSION['is_aff_id'], (86400 * 180));
    //$_COOKIE['is_aff_id'] = $_SESSION['is_aff_id'];
}


add_action( 'woocommerce_add_to_cart',  'woois_redirect_to_order_form_url', 11 );
//add_filter('add_to_cart_redirect', 'woois_redirect_to_order_form_url' , 11);
function woois_redirect_to_order_form_url() {
//error_log("woois_redirect_to_order_form_url-Start=".date('Y-m-d H:i:s'));    
    $product_id = (int) $_REQUEST['add-to-cart'];
    $is_orderform_url = get_post_meta($product_id, 'is_orderform_url', true);

    if( strlen($is_orderform_url) > 0 ){
        wp_redirect( $is_orderform_url );
        die();
    }
//error_log("woois_redirect_to_order_form_url-Ends=".date('Y-m-d H:i:s'));    
}

// Hook in
add_filter( 'woocommerce_checkout_fields' , 'isl_woois_custom_override_checkout_fields' );
// Our hooked in function - $fields is passed via the filter!
function isl_woois_custom_override_checkout_fields( $fields ) {
//error_log("isl_woois_custom_override_checkout_fields-Starts=".date('Y-m-d H:i:s'));            
    $is_settings = get_option('woocommerce_infusionsoft_settings');
    $is_settings_tags = $is_settings['is_settings_tags'];
    if(strlen($is_settings_tags) > 0){
       $fields['billing']['is_email_optin'] = array(
        'type'      => 'checkbox',
        'label'     => __('InfusionSoft Email Opt-in?', 'woocommerce'),
        'required'  => false,
        'default' => 0,
        'class'     => array('form-row-wide'),
        'clear'     => true
        );
   }
//error_log("isl_woois_custom_override_checkout_fields-Ends=".date('Y-m-d H:i:s'));            
   return $fields;
}

/**
 * Override for product page for subscription type
 */
function wcis_get_price_html($price, $product) {
//error_log("wcis_get_price_html-Starts=".date('Y-m-d H:i:s'));                                
    if (isset($_SESSION['is_aff_id'])) {
        update_post_meta(get_the_ID(), 'is_affiliate_id',$_SESSION['is_aff_id']);
    }   

    $subsType = (int)get_post_meta($product->id, 'is_subscription', true);
    $isProductID = get_post_meta($product->id, 'is_product_id', true);

    if ($subsType == 1) {
        $is_settings = get_option('woocommerce_infusionsoft_settings');
        //var_dump($is_settings);
        $app = new c4wc_iSDK;  // new instance of infusionsoft sdk
        $app->cfgCon($is_settings['is_application_name'], $is_settings['is_api_key']);       
        
        $returnFields = array('Id', 'Cycle', 'Frequency', 'PreAuthorizeAmount', 'Prorate', 'Active', 'PlanPrice');
        $query = array('ProductId' => $isProductID);
        $Subdetails = $app->dsQuery("SubscriptionPlan", 1, 0, $query, $returnFields);
        $is_sub_length = get_post_meta($product->id, 'is_sub_period', true);

        //echo "<BR>is_sub_length=".$is_sub_length."</PRE><BR>";

        $planHTML = "";
        foreach($Subdetails as $Subdetail){
            $planPrice = isset($Subdetail['PlanPrice']) ? $Subdetail['PlanPrice'] : $to_pay;
            $is_sub_length = (int)$Subdetail['Frequency'];
            $frequency = "";
            if($Subdetail['Cycle'] == 1) $frequency = "Year";
            if($Subdetail['Cycle'] == 2) $frequency = "Month";
            if($Subdetail['Cycle'] == 3) $frequency = "Week";
            if($Subdetail['Cycle'] == 6) $frequency = "Day";

            $forFrequency = $Subdetail['Frequency'];
            $planHTML .= $price . " Per " . $frequency. "   for ".$is_sub_length."   ".$frequency."(s)<BR>";
        }
        
        //Tiku- Add Offer Code Text
        $promoCode = get_post_meta($product->id, 'is_promo_code', true);
        if(strlen(($promoCode) > 0))
            $message = "<div style='width:220px;padding:10px;border:5px solid red;margin:0px;'><div style='text-decoration: blink;'><marquee>".get_post_meta($product->id, 'is_promo_code', true)."</marquee></div></div>";
        //$message .= $price . " Per " . get_post_meta($product->id, 'is_sub_period', true);
        $message .= $planHTML;
        return $message;
    }
//error_log("wcis_get_price_html-Ends=".date('Y-m-d H:i:s'));                                
    return $price;
}

add_filter('woocommerce_get_price_html', 'wcis_get_price_html', 10, 2);


/*Tiku-Add Product Option Logic-Starts*/
function getISProductOptions($app,$isProductID){
    $currency = get_woocommerce_currency_symbol();
    $productOptionsHTML = "";
    $qry = array('ProductId' => $isProductID);
    $rets = array('Id', 'ProductId','Name', 'TextMessage','OptionType');
    $productOptions = $app->dsQuery("ProductOption", 1000, 0, $qry, $rets);

    $count = 0;
    foreach($productOptions as $productOption){
        $productOptionsHTML .= '<p><label>Select Product Option: <select name="is_product_option" id="is_product_option['.$count.']" style="width: 154px;">';

        $productOptionsHTML .= '<option value=""></option>';

        $OptionId = $productOption[Id];
        $ProductId = $productOption[ProductId];
        $Name = $productOption[Name];
        $TextMessage = $productOption[TextMessage];
        $OptionType = $productOption[OptionType];
//       var_dump($productOption);

        //echo "<BR>ProductId = ".$ProductId."        Product Option Name=".$Name."       Product Option Type=".$OptionType;
        
        $qry = array('ProductOptionId' => $OptionId);
        $rets = array('Id','Label', 'Sku','Name', 'IsDefault','OptionIndex','PriceAdjustment');
        $productOptionValues = $app->dsQuery("ProductOptValue", 1000, 0, $qry, $rets);
        foreach($productOptionValues as $productOptionValue){
            $ProductOptionValueId = $productOptionValue['Id'];
            $Label = $productOptionValue['Label'];
            $Sku = $productOptionValue['Sku'];
            $Name = $productOptionValue['Name'];
            $IsDefault = $productOptionValue['IsDefault'];
            $OptionIndex = $productOptionValue['OptionIndex'];
            $PriceAdjustment = $productOptionValue['PriceAdjustment'];
            
            if(!startsWith($PriceAdjustment,"-") && $PriceAdjustment != 0)
                $PriceAdjustment = "+".$PriceAdjustment;
            //$productOptionsHTML .= "<BR>Label=".$Label."     Sku=".$Sku."       IsDefault=".$IsDefault."       OptionIndex=".$OptionIndex."       PriceAdjustment=".$PriceAdjustment;
            if($PriceAdjustment == 0)
                $productOptionsHTML .= '<option value="' . $OptionId.'_'.$ProductOptionValueId . '">' . $Label . '</option>';            
            else
                $productOptionsHTML .= '<option value="' . $OptionId.'_'.$ProductOptionValueId . '">' . $Label . ' '.$currency.$PriceAdjustment.'</option>';            
        }
        $productOptionsHTML .= '</select> </label></p>';    
        $count++;
    } 
    return $productOptionsHTML;
}
function startsWith( $haystack, $needle ){
  return $needle === ''.substr( $haystack, 0, strlen( $needle )); // substr's false => empty string
}

function endsWith( $haystack, $needle ){
  $len = strlen( $needle );
  return $needle === ''.substr( $haystack, -$len, $len ); // ! len=0
}
/*Tiku-Add Product Option Logic-Ends*/
/**
 * Tiku-Add Promocode to Checkout Page-Starts
 * Override for Checkout page for subscription type
 */
function wcis_get_price_html_checkout($cart_item, $cart_item_key) {
//error_log("wcis_get_price_html_checkout-Starts=".date('Y-m-d H:i:s'));                                    
    $productId = $cart_item_key['product_id'];
    $subsType = get_post_meta($productId, 'is_subscription', true);
    $isProductID = get_post_meta($productId, 'is_product_id', true);
    $promoCode = get_post_meta($productId, 'is_promo_code', true);
    //echo "<BR>wcis_get_price_html_checkout productTitle=".$productTitle."		Productid=".$productId ."		isProductID =".$isProductID ."		subsType=".$subsType."<BR>";

    if ($subsType == 1 && strlen($promoCode) > 0){
     $message = "<div style='width:220px;padding:5px;border:5px solid red;margin:0px;'><div style='text-decoration: blink;'><marquee>".$promoCode ."</marquee></div></div>";
     $message .= $cart_item;
     echo $message;
 }else{
  echo $cart_item;
}
//error_log("wcis_get_price_html_checkout-Ends=".date('Y-m-d H:i:s'));                                       

}

add_filter('woocommerce_cart_item_name', 'wcis_get_price_html_checkout', 10, 2);


/* Tiku-Add Promocode to Checkout Page-Ends*/

function woois_custom_cart_button_text() {
    $subsType = get_post_meta(get_the_ID(), 'is_subscription', true);
    if ($subsType == 1) {
        return __('Subscribe', 'woocommerce');
    }

    return __('Add to cart', 'woocommerce');

}

add_filter('woocommerce_product_single_add_to_cart_text', 'woois_custom_cart_button_text', 10, 0);    // 2.1 +

class WC_Infusionsoft_Admin {

    private $error = false;
    private $error_msgs = array();
    private $feedback_msgs = array();
    public $is_settings = array();
    public $infusionApp = array();
    public $is_connect_response = false;

    function __construct() {
//error_log("WC_Infusionsoft_Admin __construct-Start=".date('Y-m-d H:i:s'));        
        if (!session_id()) {
            session_start();
        }

        if (!$woo_is_product_import_map = get_option('woo_is_product_import_map')) {
            $woo_is_product_import_map = array();
        }

        $this->is_settings = get_option('woocommerce_infusionsoft_settings');
        $this->is_connect_response = false;

        if ($this->is_settings['is_application_name'] && $this->is_settings['is_api_key']) {


            $this->infusionApp = new c4wc_iSDK;

            $this->infusionApp->cfgCon($this->is_settings['is_application_name'], $this->is_settings['is_api_key']);

            //api connection check
            $response2 = $this->infusionApp->getInventory(1); //a random request. We want to catch the error not the result so the query itself doesn't matter
            if (!is_array($response2) && in_array(substr($response2, 0, 8), array('ERROR: 2', 'ERROR: 8'))) {
                $this->is_connect_response = false;
            } else {
                $this->is_connect_response = true;
            }
            //end api connection check
        }

        if ($this->is_connect_response) {
            add_action('admin_menu', array($this, 'woocommerce_admin_menu_after'), 50);
            add_action("manage_product_posts_custom_column", array($this, 'admin_column_content'), 10, 2);
            add_filter("manage_edit-product_columns", array($this, 'admin_columns'));

            add_action('save_post', array($this, 'save_post'), 10, 1);
            add_action('admin_init', array($this, 'admin_init'));

            add_action('wp_ajax_get_woo_is_tags', array($this, 'get_woo_is_tags_callback'));
        }

        // Setup the event handler for License Key Activation-Tiku
        add_action('wp_ajax_activate_woois_license_key', array($this, 'activate_woois_license_key'));
        add_action('wp_ajax_nopriv_activate_woois_license_key', array($this, 'activate_woois_license_key'));

        $plugin = plugin_basename(__FILE__); 
        add_filter("plugin_action_links", array($this, 'woois_plugin_settings_link'), 10, 2 );    
//error_log("WC_Infusionsoft_Admin __construct-Ends=".date('Y-m-d H:i:s'));
    }

    // Add settings link on plugin page
    function woois_plugin_settings_link($links, $file) { 
        if (basename($file) == "woocommerce_infusionsoft.php") {
            $l = '<a href="' . admin_url("admin.php?page=wc-settings&tab=checkout&section=wc_infusionsoft") . '">Settings</a>';
            array_unshift($links, $l);
        }
        return $links;
    }

    
    /* Tiku-Activate Licensing AJAX Code starts */

    function activate_woois_license_key() {
        $is_license_email = $_POST['is_license_email'];
        $is_license_key = $_POST['is_license_key'];
        $base_url = "http://www.informationstreet.com";
        $email = $is_license_email;
        $licence_key = $is_license_key;
        $secret_key = "woo20infusion14";
        $product_id = "wooinfusion";
        $instance = "5.1";

        $args = array(
            'request' => 'activation',
            'email' => $email,
            'licence_key' => $licence_key,
            'product_id' => $product_id,
            'secret_key' => $secret_key,
            'instance' => $instance
            );
        $licenseActivation = json_decode($this->execute_license_request($base_url, $args));
        if (strlen($licenseActivation->error) > 0) {
            update_option('IS_WOOIS_LICENSE_ACTIVATED', "");
            echo "Error Activating-[" . $licenseActivation->error . "]";
        } else {
            update_option('IS_WOOIS_LICENSE_ACTIVATED', "Y");
            echo "Your License has been updated";
        }
        //            
        die(); // this is required to return a proper result
    }

//TIKU-Licensing
    public function execute_license_request($base_url, $args) {
        $target_url = $this->create_license_url($base_url, $args);
        $data = file_get_contents($target_url);
        if(strlen($data) > 0)
            return $data;        
        else{
            $data = wp_remote_get($target_url);        
            return $data['body'];
        }
    }

//TIKU-Licensing
    // Create an url based on 
    public function create_license_url($base_url, $args) {
        $base_url = add_query_arg('wc-api', 'software-api', $base_url);
        return $base_url . '&' . http_build_query($args);
    }

    function get_woo_is_tags_callback() {
        global $wpdb; // this is how you get access to the database

        $category_id = intval($_POST['category_id']);

        $fields = array('Id', 'GroupName', 'GroupCategoryId');
        $query = array('GroupCategoryId' => $category_id);
        $result = $this->infusionApp->dsQuery('ContactGroup', 1000, 0, $query, $fields);
        asort($result);
        echo '<p><label>Select Tag: <select name="is_product_tag" id="is_product_tag" style="width: 154px;">';

        echo '<option value=""></option>';

        foreach ($result as $i => $tag) {
            echo '<option value="' . $tag['Id'] . '" ' . selected($tag['Id'], $tag_id, false) . '>' . $tag['GroupName'] . '</option>';
        }

        echo '</select> </label></p>';

        die(); // this is required to return a proper result
    }

    function admin_init() {
        add_meta_box('woo_is_meta_box', 'Infusionsoft Settings', array($this, 'product_is_settings'), 'product', 'side', 'core', 'Infusionsoft Settings');
    }

    function product_is_settings($post, $metabox) {
        global $post_ID;

        update_post_meta($post_ID, "is_product_option_ids",get_post_meta($post_ID, 'is_product_option_ids', true));
        if ($name = $metabox['args']) {
            $product_id = get_post_meta($post_ID, 'is_product_id', true);
            $category_id = get_post_meta($post_ID, 'is_product_tag_category', true);
            $tag_id = get_post_meta($post_ID, 'is_product_tag', true);
            $is_subid = get_post_meta($post_ID, 'is_subid', true);
            $is_subscription = get_post_meta($post_ID, 'is_subscription', true);
            $is_promo_code = get_post_meta($post_ID, 'is_promo_code', true);
            $is_sub_period = get_post_meta($post_ID, 'is_sub_period', true);
            $is_orderform_url = get_post_meta($post_ID, 'is_orderform_url', true);

            //    echo '<pre>';var_dump($products);echo '</pre>';
            echo '<script type="text/javascript">
            function is_product_id_changed_or_not() {
                document.getElementById("is_product_id_changed").value = 1;
            }

            function setSubscriptionOrOneOff(sel){
                var value = sel.value;  
                if(value == "0"){
                    document.getElementById("is_subid").value = -1;
                    document.getElementById("subscriptiondiv").style.display="none";

                }else{
                    document.getElementById("subscriptiondiv").style.display="block";
                    document.getElementById("is_subid").value = 1;
                }
            }
        </script>';

        echo '<p><label>Infusionsoft Product ID: </label></p><p><select onchange="is_product_id_changed_or_not();" name="is_product_id" id="is_product_id" style="width: 90%;">';
        echo "<option value=''></option>";
            //get all infusionsoft product

        $returnFields = array('Id', 'ProductName');
        $products = $this->infusionApp->dsQuery("Product", 1000, 0, array('Status' => 1), $returnFields);
        asort($products);            
        foreach ($products as $product) {
            if ($product['Id'] == $product_id) {

                $selected = 'selected="selected"';
            } else {
                $selected = "";
            }
            echo '<option ' . $selected . ' value=' . $product["Id"] . '>' . $product['ProductName'] . '</option>';
        }
        echo '</select></p>';

        $returnFields = array('Id', 'Cycle', 'Frequency', 'PreAuthorizeAmount', 'Prorate', 'Active', 'PlanPrice');
        $query = array('ProductId' => $product_id);
        $sub = $this->infusionApp->dsQuery("SubscriptionPlan", 1000, 0, $query, $returnFields);
            // echo '<p><pre>';var_dump($sub);echo '</pre></p>';

        foreach ($sub as $product) {
            $is_sub_active = $product['Active'];
            break;
        }                
        $is_sub_active_str = ($is_subscription == 0) ? "No" : "Yes";
        echo '<p><label>Is Subscription Product:  '.$is_sub_active_str.' </label></p>';                            
        $selectedSub = $is_subscription == 1 ? 'selected="selected"' : '';
        $selectedOneOff = $is_subscription == 0 ? 'selected="selected"' : '';
        echo '<p><label>Sell As:</label></p><p><select name="is_subscription" id="is_subscription" style="width: 90%;" onchange="setSubscriptionOrOneOff(this);">';
        echo '<option ' . $selectedOneOff . ' value="0">One-Off</option>';            
        echo '<option ' . $selectedSub . ' value="1">Subscription</option>';            
        echo '</select></p>';
        if ($is_sub_active == 1) {

            echo '<p><label>Infusionsoft Product SubID:</label></p><p><select name="is_subid" id="is_subid" style="width: 90%;">';

            foreach ($sub as $product) {
                $selected = $product['Id'] == $is_subid ? 'selected="selected"' : '';
                $plan = "";
                switch ($product['Cycle']) {
                        //value 1 - year, value 2 - month, value 3 - week, value 6 - day                        
                    case "1": $plan = 'year';
                    break;
                    case "2": $plan = "month";
                    break;
                    case "3": $plan = "Week";
                    break;
                    case "6": $plan = "Day";
                    break;
                    default: $plan = "Default";
                    break;
                }
                echo '<option ' . $selected . ' value=' . $product["Id"] . '>Every ' . $product['Frequency'] . ' ' . $plan . '</option>';
            }
            echo '</select></p>';
        } else {
            echo '<input type="hidden" name="is_subid" value="-1" id="is_subid" style="width: 75px;" />';
            echo '<input type="hidden" name="is_sub_period" value="0" id="is_sub_period" style="width: 75px;" />';
        }
        echo '<input type="hidden" name="is_product_id_changed"  value="-1" id="is_product_id_changed" style="width: 75px;" />';                
        if($is_subscription == 1 || $is_sub_active == 1){
            echo '<div id=subscriptiondiv><p><label>Is subscription Period: <input type="text" name="is_sub_period" value="' . $is_sub_period . '" id="is_sub_period" style="width: 75px;" /> ' . $plan . '</label></p>';
            echo '<p><label>Is Promo Code: <input type="text" name="is_promo_code" value="' . $is_promo_code . '" id="is_promo_code" style="width: 75px;" /></label></p>';
            echo '</div>';
        }
        else{
            echo '<div id=subscriptiondiv style=display:none><p><label>Is subscription Period: <input type="text" name="is_sub_period" value="' . $is_sub_period . '" id="is_sub_period" style="width: 75px;" /> ' . $plan . '</label></p>';
            echo '<p><label>Is Promo Code: <input type="text" name="is_promo_code" value="' . $is_promo_code . '" id="is_promo_code" style="width: 75px;" /></label></p>';
            echo '</div>';
        }
        echo '<p><label>OrderForm URL:</label></p><p><textarea id=is_orderform_url name=is_orderform_url rows=5 cols=30>'.$is_orderform_url.'</textarea></p>';

        echo '<script type="text/javascript">
        function is_product_id_changed_or_not() {
          document.getElementById("is_product_id_changed").value = 1;
      }
  </script>';

            //echo '<p><label>Is Promo Code: <input type="text" name="is_promo_code" value="' . $is_promo_code . '" id="is_promo_code" style="width: 75px;" /></label></p>';

  $fields = array('Id', 'CategoryName');
  $query = array('Id' => '%');
  $result = $this->infusionApp->dsQuery('ContactGroupCategory', 1000, 0, $query, $fields);
            //var_dump($result);
  asort($result);
  if (is_array($result)) {

    echo '<h3>Product Sale Tagging</h3>';
    echo '<p>When this product is purchased should we apply a tag to the contact?</p>';
    echo '<p><label>Select Tag Category: <select onchange="woo_is_request_tags();" name="is_product_tag_category" id="is_product_tag_category" style="width: 100px;">';

    $category_id = strlen($category_id) > 0 ? $category_id : 0;
    $selected1 = $category_id == 0 ? 'selected' : '';
    echo '<option value="0" ' . $selected1 . '>Uncategorized</option>';

    echo '<option value="-1">------------------</option>';
    foreach ($result as $i => $category) {
        if (trim($category['CategoryName'])) {
            echo '<option value="' . $category['Id'] . '" ' . selected($category['Id'], $category_id, false) . '>' . $category['CategoryName'] . '</option>';
        }
    }

    echo '</select> <img style="display: none;" class="woo_is_ajax_loader" src="' . trailingslashit(plugin_dir_url(__FILE__)) . 'ajax-loader.gif' . '" /></label></p>';

    echo '<script type="text/javascript">
    function woo_is_request_tags() {
      jQuery(".woo_is_ajax_loader").show();
      jQuery(".woo_is_product_tag_selection_container").hide();
      jQuery(".woo_is_product_tag_selection_container p").remove();

      var data = {
         action: "get_woo_is_tags",
         category_id: jQuery("#is_product_tag_category").val()
     };

     jQuery.post(ajaxurl, data, function(response) {
         jQuery(".woo_is_ajax_loader").hide();
         jQuery(".woo_is_product_tag_selection_container").html(response).fadeIn();
     });
}
</script>';

echo '<div class="woo_is_product_tag_selection_container">';

if ($category_id) {
    $fields = array('Id', 'GroupName', 'GroupCategoryId');
    $query = array('GroupCategoryId' => $category_id);
    $result = $this->infusionApp->dsQuery('ContactGroup', 1000, 0, $query, $fields);
    asort($result);
    echo '<p><label>Select Tag: <select name="is_product_tag" id="is_product_tag" style="width: 154px;">';

    echo '<option value=""></option>';

    foreach ($result as $i => $tag) {
        echo '<option value="' . $tag['Id'] . '" ' . selected($tag['Id'], $tag_id, false) . '>' . $tag['GroupName'] . '</option>';
    }

    echo '</select> </label></p>';
}

echo '</div>';
}

            //Add Affiliate Link Generation text
echo '<h3>Infusionsoft Affiliate Link Setup</h3>';
$thisPageURL = get_permalink();
echo "<p>If you would like to set up an Infusionsoft Redirect Link for this page simply go to <a href=https://aml.infusionsoft.com/Jumper/manageJumper.jsp?view=add>Here</a> and use the following as the Website Address for the Redirect Link.
<a href=https://".$this->is_settings['is_application_name'].".infusionsoft.com/aff.html?to=".$thisPageURL.">Affiliate Redirect Link</a>
</p>";


}
}

function save_post($product_id) {
    if (!wp_is_post_revision($product_id)) {
        if (@$_POST['is_product_id']) {
            $meta_is_product_id = get_post_meta($product_id, 'is_product_id', true);
            $is_product_id_changed = $_POST['is_product_id_changed'];
            $isproductId = ($is_product_id_changed == 1 || strlen($meta_is_product_id) <= 0)  ?
            $_POST['is_product_id'] : $meta_is_product_id; 
            update_post_meta($product_id, 'is_product_id', $isproductId);
                //update_post_meta($product_id, 'is_product_id', $_POST['is_product_id']);
        }

        if (@$_POST['is_product_tag_category']) {
            update_post_meta($product_id, 'is_product_tag_category', $_POST['is_product_tag_category']);
        }

        if (@$_POST['is_product_tag']) {
            update_post_meta($product_id, 'is_product_tag', $_POST['is_product_tag']);
        }
        if (@$_POST['is_subscription'] == 1) {
            if (@$_POST['is_subid']) {
                update_post_meta($product_id, 'is_subid', $_POST['is_subid']);
            }

            if (@$_POST['is_subscription']) {
                update_post_meta($product_id, 'is_subscription', $_POST['is_subscription']);
            }

            if (@$_POST['is_sub_period']) {
                update_post_meta($product_id, 'is_sub_period', $_POST['is_sub_period']);
                update_post_meta($product_id, 'is_sub_length', $_POST['is_sub_length']);
            }
        } else {
            update_post_meta($product_id, 'is_subid', -1);
            update_post_meta($product_id, 'is_subscription', 0);
            update_post_meta($product_id, 'is_sub_period', " ");
        }
        if (@$_POST['is_promo_code']) {
            update_post_meta($product_id, 'is_promo_code', $_POST['is_promo_code']);
        }
        if (@$_POST['is_orderform_url']) {
            update_post_meta($product_id, 'is_orderform_url', $_POST['is_orderform_url']);
        }   
            /*if (@$_POST['is_product_option_ids']) {
                update_post_meta($product_id, 'is_product_option_ids', $_POST['is_product_option_id']);
            }*/             
        }
/*//error_log("###Settings Variations");
        //made it variable but variations wont be added!
        $skuu = rand();
        update_post_meta($product_id, '_sku', $skuu );
        //my array for environment the attributes
        update_post_meta( $product_id, '_price', "25" );

        $thedata = array(
                    'pa_size'=> array(
                        'identify'=>'pa_size',
                        'value'=>'',
                        'is_visible' => '1',
                        'is_variation' => '1',
                        'is_taxonomy' => '1'
                        )
                  );
        update_post_meta( $product_id,'_product_attributes',$thedata);
        //made it variable but variations wont be added!
        wp_insert_term('XL', 'pa_size');        
        wp_set_object_terms ($product_id, 'variable', 'product_type');
        wp_set_object_terms( $product_id, 'XL', 'pa_size' );
        
        update_post_meta( $product_id, '_visibility', 'visible' );
        update_post_meta( $product_id, '_stock_status', 'instock');*/
        
    }

    public function get_user() {
        get_currentuserinfo();
        global $current_user, $post;
        $user_id = $current_user->ID;
        return $user_id;
    }

    function admin_columns($cols) {
        $cols['is_product_id'] = 'IS Prod ID';

        return $cols;
    }

    function admin_column_content($field, $id = false) {

        if ($field == 'is_product_id') {
            echo get_post_meta($id, 'is_product_id', true);
        }
    }

    function woocommerce_admin_menu_after() {
        add_submenu_page('woocommerce', __('Infusionsoft Import', 'woocommerce'), __('Infusionsoft Import', 'woocommerce'), 'manage_woocommerce', 'woo_is_importer', array($this, 'generate_options_page'));
    }

    public function start_row($additional_style = false) {
        return '<div style="margin-bottom: 10px;' . $additional_style . '">';
    }

    public function end_row() {
        return '</div>';
    }

    public function post($name, $default = false) {
        $var = (isset($_POST[$name]) ? $_POST[$name] : $default);
        return $var;
    }

    public function start_box($title, $return = true) {
        $html = '   <div class="postbox" style="margin: 5px 0px;">
        <h3>' . $title . '</h3>
        <div class="inside">';

            if ($return) {
                return $html;
            } else {
                echo $html;
            }
        }

        public function end_box($return = true) {
            $html = '       </div>
        </div>';

        if ($return) {
            return $html;
        } else {
            echo $html;
        }
    }

    public function add_error($msg) {
        $this->error_msgs[] = $msg;
        $this->error = true;
    }

    public function is_error() {
        return $this->error;
    }

    public function add_feedback($msg) {
        $this->feedback_msgs[] = $msg;
    }

    public function immediate_feedback($msg, $return = true) {
        $html = '<div style="margin: 10px 0px;">';
        $html .= '<div style="color: #000000; background-color:#CCFFCC; border: 1px solid #336633; padding: 8px; margin: 0px 10px 5px 0px;">' . $msg . '</div>';
        $html .= '</div>';

        if ($return) {
            return $html;
        } else {
            echo $html;
        }
    }

    public function display_feedback($return = true) {
        $html = '';

        if (count($this->feedback_msgs)) {
            $html .= '<div style="margin: 10px 0px;">';
            foreach ($this->feedback_msgs as $msg) {
                $html .= '<div style="color: #000000; background-color:#CCFFCC; border: 1px solid #336633; padding: 8px; margin: 0px 10px 5px 0px;">' . $msg . '</div>';
            }
            $html .= '</div>';
        }

        if ($return) {
            return $html;
        } else {
            echo $html;
        }
    }

    public function display_errors($return = true) {
        $html = '';

        if (count($this->error_msgs)) {
            $html .= '<div style="margin: 10px 0px;">';
            foreach ($this->error_msgs as $msg) {
                $html .= '<div style="color: #333333; background-color:#FFEBE8; border: 1px solid #CC0000; padding: 8px; margin: 0px 10px 5px 0px;">' . $msg . '</div>';
            }
            $html .= '</div>';
        }

        if ($return) {
            return $html;
        } else {
            echo $html;
        }
    }

    public function draw_text($name, $value = false, $style = false, $class = false, $caption = false, $args = false) {
        if (!$style) {
            $style = 'width: 200px; padding: 5px;';
        }

        $field = '<input id="' . $name . '" type="text" value="' . $value . '" style="' . $style . '" ' . ($args ? $args : '') . ' class="input ' . $class . '" name="' . $name . '" />' . ($caption ? $this->get_block_caption($caption) : '');

        return $field;
    }

    public function draw_text_row($label, $name, $value = false, $style = false, $class = false, $caption = false, $args = false) {
        $html = '';

        $html .= $this->start_row('margin-bottom: 10px;');
        $html .= '<label><div style="float: left; margin-right: 30px; margin-top: 8px; width: 200px;">' . $label . '</div><div style="float: left;">';
        $html .= $this->draw_text($name, $value, $style, $class, false, $args) . ($caption ? $this->get_block_caption($caption) : '');
        $html .= '</div></label>';
        $html .= $this->draw_clearing_div();
        $html .= $this->end_row();

        return $html;
    }

    public function get_inline_caption($text) {
        return '<span style="color: gray; margin-left: 10px;">' . $text . '</span>';
    }

    public function draw_submit($name, $value, $style = false, $class = false) {
        return '<input type="submit" name="' . $name . '" class="' . $class . ' button-primary" value="' . $value . '" style="' . $style . '" />';
    }

    public function draw_hidden($name, $value = false) {
        return '<input type="hidden" value="' . $value . '" name="' . $name . '" />';
    }

    public function get_block_caption($text) {
        return '<div style="color: gray; margin-top: 5px; width: 500px;">' . $text . '</div>';
    }

    public function draw_clearing_div() {
        return '<div style="clear: both; padding: 0px; margin: 0px; height: 1px;">&nbsp;</div>';
    }

    public function display_messages($return = true) {
        $html = '';

        $html .= $this->display_errors();
        $html .= $this->display_feedback();

        if ($return) {
            return $html;
        } else {
            echo $html;
        }
    }

    public function display_message($msg, $type = 'feedback', $return = true) {
        $class = ($type == 'feedback' ? 'updated fade' : 'error');
        $html = '	<div id="message" class="' . $class . '" style="clear: both; margin-top: 5px; padding: 7px;">
        ' . $msg . '
    </div>';

    if ($return) {
        return $html;
    } else {
        echo $html;
    }
}

public function draw_textarea_short_row($label, $name, $value = false, $style = false, $class = false, $caption = false) {
    $html = '';

    $html .= $this->start_row('margin-bottom: 10px;');
    $html .= '<label><div style="float: left; margin-right: 30px; margin-top: 8px; width: 200px;">' . $label . '</div><div style="float: left;">';
    $html .= $this->draw_textarea_short($name, $value, $style, $class) . ($caption ? $this->get_block_caption($caption) : '');
    $html .= '</div></label>';
    $html .= $this->draw_clearing_div();
    $html .= $this->end_row();

    return $html;
}

public function draw_textarea_short($name, $value = false, $style = false, $class = false, $caption = false) {
    if (!$style) {
        $style = 'width: 370px; height: 80px;';
    }

    return '<textarea style="' . $style . '" class="input ' . $class . '" name="' . $name . '">' . htmlentities($value) . '</textarea>' . ($caption ? $this->get_block_caption($caption) : '');
}

public function draw_textarea_row($label, $name, $value = false, $style = false, $class = false, $caption = false) {
    $html = '';

    $html .= $this->start_row('margin-bottom: 10px;');
    $html .= '<label><div style="float: left; margin-right: 30px; margin-top: 8px; width: 200px;">' . $label . '</div><div style="float: left;">';
    $html .= $this->draw_textarea($name, $value, $style, $class) . ($caption ? $this->get_block_caption($caption) : '');
    $html .= '</div></label>';
    $html .= $this->draw_clearing_div();
    $html .= $this->end_row();

    return $html;
}

public function printr($array, $return = false) {
    $html = '<pre>';
    $html .= print_r($array, true);
    $html .= '</pre>';

    if ($return) {
        return $return;
    } else {
        echo $html;
    }
}

function yesno_image($var) {
    return '<img src="' . trailingslashit(plugin_dir_url(__FILE__)) . ($var ? 'tick.gif' : 'cross.gif') . '" />';
}

function process_import() {
    global $wpdb, $current_user, $woo_is_product_import_map;
    if ($this->post('woo_is_import')) {
        if ($import_ids = $this->post('is_product_import')) {
                //delete_transient('is_product_import_cache');
                //$query = array('Id' => '%');
                //$fields = array('Id', 'ProductName', 'ProductPrice', 'Sku', 'ShortDescription', 'Taxable', 'Description', 'Status', 'LargeImage', 'TopHTML', 'BottomHTML', 'Weight');

            $has_results = true;
            $product_count = 0;
            $page = 0;
            $pre_existing_products = 0;
            $products_array = array();

                //while ($has_results) {
                //if ($page >= 5) { //we want to import 5000 at a time.
                //$this->add_error('Got to 5,000 results from Infusionsoft so stopping the query. This might mean that if your WooCommerce store has more than 20,000 products then any above that number might not get in. But check the result as they should all have imported');
                //break;
                //}

            if ($products = $this->query_products_from_is(4)) {
                if (is_array($products)) {
                        //$has_results = false;
                        //break;

                    foreach ($products as $i => $product) {
                        if (@$product['ProductName']) {
                            $name_sql = '	SELECT ID
                            FROM ' . $wpdb->posts . '
                            WHERE
                            post_type = "product"
                            AND post_status = "publish"
                            AND post_title = "' . $product['ProductName'] . '"';

                            $product['sku_match'] = 0;
                            if ($product['Sku'] = @$product['Sku']) {
                                $sku_sql = '	SELECT ID
                                FROM
                                ' . $wpdb->posts . ' p
                                JOIN ' . $wpdb->postmeta . ' pm ON (
                                  p.ID = pm.post_id
                                  AND pm.meta_key = "_sku"
                                  )
WHERE
post_type = "product"
AND post_status = "publish"
AND LOWER(meta_value) = "' . strtolower($product['Sku']) . '"';

$product['sku_match'] = (int) $wpdb->get_var($sku_sql);
}

if (isset($woo_is_product_import_map[$product['id']])) {
    $product['id_match'] = $woo_is_product_import_map[$product['id']];
} else {
    $id_sql = '	SELECT ID
    FROM
    ' . $wpdb->posts . ' p
    JOIN ' . $wpdb->postmeta . ' pm ON (
      p.ID = pm.post_id
      AND pm.meta_key = "is_product_id"
      )
WHERE
post_type = "product"
AND post_status = "publish"
AND meta_value = "' . $product['Id'] . '"';

if ($product['id_match'] = (int) $wpdb->get_var($id_sql)) {
                                        //$woo_is_product_import_map[$product['id']] = $product['id_match'];
    $pre_existing_products++;
}
}
$product['name_match'] = (int) $wpdb->get_var($name_sql);

$products_array[$product['Id']] = $product;
}

$product_count++;
}
}
}
                //$page++;
                //}

$imported = 0;

foreach ($import_ids as $import_id) {
                    //if (isset($woo_is_product_import_map[$import_id])) {
                    //$this->add_error('Skipping product "' . $product['ProductName'] . '". Already in the database');
                    //continue;
                    //}
                    //if ($imported >= 1000) {
                    //$this->add_feedback('Imported 1000 products. To import more please press import once again.');
                    //break;
                    //}

    if ($product = $products_array[$import_id]) {
        if ($product['id_match']) {
            $this->add_error('Skipping product "' . $product['ProductName'] . '". Already in the database');
        } else if ($product['sku_match']) {
            update_post_meta($product['sku_match'], 'is_product_id', $import_id);
                            //$woo_is_product_import_map[$import_id] = $product['sku_match'];
            $this->add_feedback('Linked product "' . $product['ProductName'] . '" by SKU');
        } else if ($product['name_match']) {
            update_post_meta($product['name_match'], 'is_product_id', $import_id);
                            //$woo_is_product_import_map[$import_id] = $product['name_match'];
            $this->add_feedback('Linked product "' . $product['ProductName'] . '" by Name (<a target="_blank" href="' . trailingslashit(admin_url()) . 'post.php?action=edit&post=' . $product['name_match'] . '">Product ' . $product['name_match'] . '</a> linked to IS product ' . $import_id . ')');
        } else {
                            // Create post object
            $my_post = array(
                'post_title' => $product['ProductName'],
                'post_content' => @$product['Description'],
                'post_type' => 'product',
                'post_status' => 'publish',
                'post_author' => $current_user->ID,
                );

                            // Insert the post into the database
            if ($product_id = wp_insert_post($my_post)) {
                update_post_meta($product_id, '_visibility', 'visible');
                update_post_meta($product_id, '_sku', $product['Sku']);
                update_post_meta($product_id, '_price', $product['ProductPrice']);
                update_post_meta($product_id, '_regular_price', $product['ProductPrice']);
                update_post_meta($product_id, 'is_product_id', $import_id);
                update_post_meta($product_id, '_weight', $product['Weight']);



                                //$woo_is_product_import_map[$import_id] = $product_id; //for the mapping table
                                //image
                require_once(ABSPATH . 'wp-admin' . '/includes/image.php');
                require_once(ABSPATH . 'wp-admin' . '/includes/file.php');
                require_once(ABSPATH . 'wp-admin' . '/includes/media.php');

                                // upload image to server
                if (@$product['LargeImage']) {
                    media_sideload_image('https://' . $this->is_settings['is_application_name'] . '.infusionsoft.com/cart/pimg.jsp?s=large&i=' . $product['Id'] . '&t=p&type=.png', $product_id);

                                    // get the newly uploaded image
                    $attachments = get_posts(array(
                        'post_type' => 'attachment',
                        'number_posts' => 1,
                        'post_status' => null,
                        'post_parent' => $product_id,
                        'orderby' => 'post_date',
                        'order' => 'DESC',)
                    );

                    if (is_array($attachments) && isset($attachments[0])) {
                        set_post_thumbnail($product_id, $attachments[0]->ID);
                        $feedback_end = 'Image was attached';
                    } else {
                        $feedback_end = 'Image failed to upload';
                    }
                } else {
                    $feedback_end = 'No image was provided by Infusionsoft';
                }
                                //end image

                $this->add_feedback('Added new product "' . $product['ProductName'] . '". ' . $feedback_end);
            }
        }

        $imported++;
    }

                    //Test
                    //break;
}

                //update_option('woo_is_product_import_map', $woo_is_product_import_map);
}
}
}



function query_products_from_is($pages = 1) {
    $query = array('Id' => '%');
    $fields = array('Id', 'ProductName', 'ProductPrice', 'Sku', 'Taxable',   'Status', 'LargeImage', 'Weight', 'Description', 'ShortDescription');
    $products = array();

    $products_page = $this->infusionApp->dsQuery('Product', (isset($_GET['high_volume']) ? 1000 : 500), 0, $query, $fields);
        /*echo '<pre>';
        print_r($products_page, true);
        echo '</pre>';
        die;
        */
        //delete_transient('is_product_api_import_cache');
        //if (!$products = get_transient('is_product_api_import_cache')) {
        for ($i = 0; $i < $pages; $i++) {
            if ($products_page = $this->infusionApp->dsQuery('Product', (isset($_GET['high_volume']) ? 1000 : 500), (int) $i, $query, $fields)) {
                /*echo '<pre>';
                print_r($products_page, true);
                echo '</pre>';
                die;*/

                if (!is_array($products_page)) {
                    break;
                } else {
                    foreach ($products_page as $product) {
                        $products[] = $product;
                    }
                }
            }
        }

        //set_transient('is_product_api_import_cache', $products, 3600);
        //} else {
        //$this->printr($products);
        //$products = unserialize($products);
        //}

        return $products;
    }

    function generate_options_page() {
        global $wpdb, $woo_is_product_import_map;

        $this->process_import();
        $html = '';

        //if (!$products_array = get_transient('is_product_import_cache')) {
        set_time_limit(0); //600 seconds. ten minutes

        $products_array = array();
        $page = 0;
        //$has_results = true;
        $product_count = 0;
        $pre_existing_products = 0;
        $products_no_name = 0;
        $total_products = 0;

        //while ($has_results) {
        //if ($page >= 40) {
        //echo '<p><strong>Stopping querying the API because we got to 40 pages which equates to 40,000 products. This script can handle it but a limit has been set to prevent perpetual page load times.</strong></p>';
        //break;
        //}
        //if ($product_count >= 1000) {
        //echo $this->immediate_feedback('Here are 1000 of your products. Click to import these and, once done, the page will show the next 1000. This will continue until the WooCommerce database has your product data in full.');
        //break;
        //}
        //if ($products = $this->infusionApp->dsQuery('Product', 1000, $page, $query, $fields)) {
        //if (!is_array($products)) {
        //$has_results = false;
        //break;
        //}
        //}

        $products = $this->query_products_from_is((isset($_GET['high_volume']) ? 4 : 1));

        if (is_array($products)) {
            foreach ($products as $i => $product) {
                //if (isset($woo_is_product_import_map[$product['Id']])) {
                //continue; //already have this product in our database
                //}

                if (@$product['ProductName']) {
                    if (@$product['LargeImage']) {
                        $product['LargeImage'] = 1;
                    } else {
                        $product['LargeImage'] = 0;
                    }


                    $name_sql = '	SELECT ID
                    FROM ' . $wpdb->posts . '
                    WHERE
                    post_type = "product"
                    AND post_status = "publish"
                    AND post_title = "' . $product['ProductName'] . '"';

                    $product['sku_match'] = 0;
                    if ($product['Sku'] = @$product['Sku']) {
                        $sku_sql = '	SELECT ID
                        FROM
                        ' . $wpdb->posts . ' p
                        JOIN ' . $wpdb->postmeta . ' pm ON (
                            p.ID = pm.post_id
                            AND pm.meta_key = "_sku"
                            )
WHERE
post_type = "product"
AND post_status = "publish"
AND LOWER(meta_value) = "' . strtolower($product['Sku']) . '"';

$product['sku_match'] = (int) $wpdb->get_var($sku_sql);
}

$id_sql = '	SELECT ID
FROM
' . $wpdb->posts . ' p
JOIN ' . $wpdb->postmeta . ' pm ON (
 p.ID = pm.post_id
 AND pm.meta_key = "is_product_id"
 )
WHERE
post_type = "product"
AND post_status = "publish"
AND meta_value = "' . $product['Id'] . '"';

if ($product['id_match'] = (int) $wpdb->get_var($id_sql)) {
    $pre_existing_products++;
                        //continue; //ignore because it is already imported
}

$product['name_match'] = (int) $wpdb->get_var($name_sql);

$products_array[$product['Id']] = $product;

$product_count++;
} else {
    $products_no_name++;
}

$total_products++;
}
}

        //$page++;
        //}


ksort($products_array);

        //set_transient( 'is_product_import_cache', serialize($products_array), 3600 ); //an hour
        //} else {
        //delete_transient( 'is_product_import_cache');
        //$products_array = unserialize($products_array);
        //$product_count = count($products_array);
        //}
        //echo '<pre>';
        //print_r($products_array);
        //echo '</pre>';

$label_style = 'style="float: left; margin-right: 30px; margin-top: 8px; font-weight: bold; width: 150px;"';
$style = 'display: inline-block; margin-right: 5px; margin-left: 5px;';

$html .= $this->display_messages();

$html .= '<div class="wrap" id="poststuff" style="margin-bottom: 10px;">
<h2>WooCommerce Infusionsoft Product Importer</h2>
<p>This page allows you to import products from Infusionsoft into WooCommerce. Enter your API information into the WooCommerce payment gateways page and then visit this page to be able to selectively import from Infusionsoft.</p>
<p>The table below gives a list of the products. The three match fields relate to how the system will attempt to establish if a product has already been imported. The SKU and Name fields will be directly queried. Checking the box and running the import will ONLY link the existing product to its respective IS ID number. If an IS Product ID is found attached to a product then it will not allow you to import. Otherwise using the checkbox will add an entirely new product and link the ID to the IS reference.</p>
<p>NOTE: This importer will only pull the first 5000 products from your Infusionsoft account. If you have more than this then please contact us to increase that. Note that the more products you have the slower this will be. On averate to parse 5000 products it will take 5 or 6 minutes. The page is set to time out after ten minutes to prevent the server potentially locking up (Very rare and nothing to worry about).</p>';

$html .= '<form method="POST" style="margin-left: 6px;">';

$html .= $this->start_box('Product Import Stats');

$html . '<p><strong>' . $total_products . ' product' . ($total_products == 1 ? '' : 's') . ' were queried from the IS account</strong></p>';

if ($pre_existing_products) {
    $html .= '<p>Found ' . $pre_existing_products . ' product' . ($pre_existing_products == 1 ? '' : 's') . ' which will not be imported because they are already in your database (matched on Infusionsoft Product ID)</p>';
}

if ($products_no_name) {
    $html .= '<p>Found ' . $products_no_name . ' product' . ($products_no_name == 1 ? '' : 's') . ' which cannot be imported because they have no name</p>';
}

        //$max_import_product_count = $total_products - $products_no_name - $pre_existing_products;
        //$html .= '<p>A total of ' . $max_import_product_count . ' product' . ($max_import_product_count == 1 ? '':'s') . ' can be imported on this occasion.</p>';

$html .= $this->end_box();

$html .= $this->start_box('Product Import');

        //$html .= '<p>Queried ' . $product_count . ' product' . ($product_count == 1 ? '':'s') . ' from Infusionsoft</p>';
        //$html .= '<p>' . (int)$pre_existing_products . ' product' . ($pre_existing_products == 1 ? '':'s') . ' are already in the database. There may be more matches.</p>';
        //$html .= '<p>Mapped ' . count($woo_is_product_import_map) . ' product' . (count($woo_is_product_import_map) == 1 ? '':'s') . ' so far.</p>';

$html .= $this->start_row('margin-bottom: 10px;');

if ($products_array) {
    $html .= $this->start_row('margin-bottom: 10px;');
    $html .= $this->draw_submit('woo_is_import', 'Import Products');
    $html .= $this->draw_clearing_div();
    $html .= $this->end_row();

    $html .= '<table class="widefat"><thead>';

    $html .= '<tr>
    <th style="text-align: center;"><input style="margin: 0px;" type="checkbox" value="All" onclick=" jQuery(this).closest(\'table\').find(\':checkbox\').prop(\'checked\', this.checked);" /></th>
    <th style="text-align: center;">ID</th>
    <th style="text-align: center;">ID Match</th>
    <th style="text-align: center;">SKU Match</th>
    <th style="text-align: center;">Name Match</th>
    <th>Product Name</th>
    <th>SKU</th>
    <th>Price</th>
    <th>Image</th>
</tr></thead><tbody>';

foreach ($products_array as $i => $product) {
    $existing_link = false;

    if ($product['id_match']) {
        $existing_link = '/wp-admin/post.php?post=' . $product['id_match'] . '&action=edit';
    }

    $html .= '<tr>
    <td style="text-align: center;">' . ($product['id_match'] ? '' : '<input type="checkbox" class="import_product" name="is_product_import[' . $product['Id'] . ']" value="' . $product['Id'] . '" />') . '</td>
    <td style="text-align: center;">' . ($existing_link ? '<a target="_blank" href="' . $existing_link . '">' : '') . $product['Id'] . ($existing_link ? '</a>' : '') . '</td>
    <td style="text-align: center;">' . $this->yesno_image($product['id_match']) . '</td>
    <td style="text-align: center;">' . ($product['Sku'] ? $this->yesno_image($product['sku_match']) : '') . '</td>
    <td style="text-align: center;">' . $this->yesno_image($product['name_match']) . '</td>
    <td>' . $product['ProductName'] . '</td>
    <td>' . $product['Sku'] . '</td>
    <td>' . number_format($product['ProductPrice'], 2) . '</td>
    <td>' . (@$product['LargeImage'] ? '<img style="width: 50px;" src="https://' . $this->is_settings['is_application_name'] . '.infusionsoft.com/cart/pimg.jsp?s=small&i=' . $product['Id'] . '&t=p"/>' : '') . '</td>
</tr>';
}

$html .= '</tbody></table>';

$html .= $this->start_row('margin-top: 10px;');
$html .= $this->draw_submit('woo_is_import', 'Import Products');
$html .= $this->draw_clearing_div();
$html .= $this->end_row();
} else {
    $html .= '<p>There are no products in the IS database for import</p>';
}

$html .= $this->end_row();

$html .= $this->end_box();

$html .= '</form>';

$html .= '</div>';

echo $html;
}

}

if ( is_admin() ) {
    $wc_is_admin = new WC_Infusionsoft_Admin();
}

function woo_is_call_function($function) {
    global $wc_is_admin;

    echo $wc_is_admin->$function();
}

class WC_Infusionsoft extends WC_Payment_Gateway {

    public function __construct() {
//error_log("WC_Infusionsoft __construct-Start=".date('Y-m-d H:i:s'));                
        global $woocommerce;

        $this->id = 'infusionsoft';
        $this->icon == apply_filters('woocommerce_infusionsoft_icon', plugins_url('infusionsoft.png', __FILE__));
        $this->has_fields = true;
        $this->method_title = __('Infusionsoft', 'woocommerce');
        $this->api_connected = true;

        // Load the form fields.
        $this->init_form_fields();

        // Load the settings.
        $this->init_settings();

        //print_r($this->settings);
        // Define user set variables
        $this->title = $this->settings['title'];
        $this->cards = ($this->settings['cards'] ? $this->settings['cards'] : "VISA\nMASTERCARD");
        $this->wooorderstatus = $this->settings['wooorderstatus'];
        $this->description = $this->settings['description'];
        $this->debug = $this->settings['debug'];
        $this->debug_email = $this->settings['debug_email'];
        $this->order_customflds = $this->settings['order_customflds'];
        $this->order_product_customfld = $this->settings['order_product_customfld'];
        $this->order_customtable = $this->settings['order_customtable'];
        $this->tax_label = $this->settings['tax_label'];
        $this->thanks_message = $this->settings['thanks_message'];
        $this->is_application_name = $this->settings['is_application_name']; //defaults to empty
        $this->is_api_key = $this->settings['is_api_key']; //defaults to empty
        $this->is_merchant_id = $this->settings['is_merchant_id']; //defaults to empty
        $this->test_mode = $this->settings['test_mode']; //defaults to empty
        $this->is_settings_tags = $this->settings['is_settings_tags']; //defaults to empty
        $this->is_free_shipping = $this->settings['is_free_shipping']; //defaults to empty

        $this->api_info = ''; //defaults to empty

        $this->api_connected = 0;

        if ($this->is_application_name && $this->is_api_key) {
            //api connection check
            include_once(plugin_dir_path(__FILE__) . 'isdk.php');
            $infusionApp = new c4wc_iSDK;
            $response = $infusionApp->cfgCon($this->is_application_name, $this->is_api_key);
            /*$response2 = $infusionApp->getInventory(1); //a random request. We want to catch the error not the result so the query itself doesn't matter
            if (!is_array($response2) && in_array(substr($response2, 0, 8), array('ERROR: 2', 'ERROR: 8'))) {
                $this->api_connected = 0;
            } else {
                $this->api_connected = 1;
            }*/
            try{
              $connected = $infusionApp->dsGetSetting("Contact","optiontypes");
              $this->api_connected = 1;
          }catch (Exception $e){
              $this->api_connected = 0;
          }               
            //end api connection check
      }

      if ($this->is_application_name && $this->is_api_key) {
        $api_info = 'InfusionsoftApp:' . $this->is_application_name . ':i:' . $this->is_api_key . ':This is the API key for ' . $this->is_application_name . '.infusionsoft.com.';
        $this->api_info = $api_info;
    }

    $this->feedback_message = '';

        // Actions
    if (version_compare(WOOCOMMERCE_VERSION, '2.0', '<')) {
            // Pre 2.0
        add_action('woocommerce_update_options_payment_gateways', array(&$this, 'process_admin_options'));
    } else {
            // 2.0
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
    }


    add_action('woocommerce_thankyou_' . $this->id, array(&$this, 'thankyou_page'));
    if ($this->debug == 'yes'){
            //$this->log = $woocommerce->logger();
        $this->log = new WC_Logger();
    }

    if (!$this->is_valid_for_use())
        $this->enabled = false;
//error_log("WC_Infusionsoft __construct-Ends=".date('Y-m-d H:i:s'));                        
}

function is_valid_for_use() {

    return true;
}

function admin_options() {
//error_log("admin_options-Start=".date('Y-m-d H:i:s'));   
    /* Tiku-Licensing HTML Starts */
    update_option("IS_WOOIS_LICENSE_ACTIVATED", "Y");
    if (strlen(get_option("IS_WOOIS_LICENSE_ACTIVATED")) <= 0) {
        submit_button();
            //echo '<p><a href=http://www.informationstreet.com/lost-licence/ target=_blank>Lost your License?Find it Here</a></p>';
        echo '<h3>Woocommerce-InfusionSoft:Activate your License(<a href=http://www.informationstreet.com/lost-licence/ target=_blank>Lost your License?Find it Here</a>)</h3>';
        echo '<img style="display: none;" class="wisp_is_ajax_loader" src="' . trailingslashit(plugin_dir_url(__FILE__)) . 'ajax-loader.gif' . '" /></label></p>';
        echo '<script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery(".submit").hide();
        });                

function activateLicense() {
    jQuery(".wisp_is_ajax_loader").show();
    var data = {
        action: "activate_woois_license_key",
        is_license_email: jQuery("#is_license_email").val(),
        is_license_key: jQuery("#is_license_key").val()
    };

    jQuery.post(ajaxurl, data, function(response) {
        alert("response="+response);
        jQuery(".wisp_is_ajax_loader").hide();
        if(response.trim().indexOf("Error") != 0){
            location.reload(true);
        }
    });
}
</script>';
echo '<table class="form-table">';
echo '<tr"> 
<th><label for="is_license_email">Your Email Id for License</label></th> 
<td>';
    echo '<input type="text" name="is_license_email" id="is_license_email" value="" size="50" required/> 
    <br /><span class="description">Your Email Id for License.</span>';
    echo '</td></tr>';

    echo '<tr"> 
    <th><label for="is_license_key">Your License Key</label></th> 
    <td>';
        echo '<input type="text" name="is_license_key" id="is_license_key" value="" size="50" required/> 
        <br /><span class="description">Your License Ley.</span>';
        echo '</td></tr>';

        echo '<tr">
        <td>';
            echo '<input type="button" class="button-primary" name="is_activate" id="is_license_key" value="Activate License" size="50" onclick="activateLicense();" />';
            echo '</td></tr>';

            echo "</table>";
        } else {
            echo '<h3>' . __('Infusionsoft', 'woocommerce') . '</h3>';
            echo '<table class="form-table">';

            if (!$this->api_info || !$this->api_connected) {
                echo '<div class="inline error"><p><strong>' . __('Infusionsoft Link Disabled', 'woocommerce') . '</strong>: ' . __('The Infusionsoft module will function once the complete & correct API info has been added.', 'woocommerce') . '</p></div>';
            }

            $this->generate_settings_html();

            echo '</table>';
        }
//error_log("admin_options-Ends=".date('Y-m-d H:i:s'));           
    }

    function is_available() {
        $return = true;

        if (!$this->is_merchant_id) {
            $return = false;
        }

        return $return;
    }

    function init_form_fields() {
//error_log("init_form_fields-Starts=".date('Y-m-d H:i:s'));                           
        $this->form_fields = array(
            'enabled' => array(
                'title' => __('Enable/Disable', 'woocommerce'),
                'type' => 'checkbox',
                'label' => __('Enable Infusionsoft', 'woocommerce'),
                'default' => 'yes'
                ),
            'test_mode' => array(
                'title' => __('Test Mode?', 'woocommerce'),
                'type' => 'checkbox',
                'description' => __('When turned on the system will error just before it charges the card. This will enable you to test the rest of the system including the card validation and product/client insertion', 'woocommerce'),
                'default' => 'no'
                ),
            'is_application_name' => array(
                'title' => __('Infusionsoft Application Name', 'woocommerce'),
                'type' => 'text',
                'description' => __('The Infusionsoft application name. This is the part before the Infusionsoft domain on your site. Eg: the XXX from XXX.infusionsoft.com.', 'woocommerce'),
                'default' => ''
                ),
            'is_api_key' => array(
                'title' => __('Infusionsoft API Key', 'woocommerce'),
                'type' => 'text',
                'description' => __('This controls access to Infusionsoft. Can be found in your admin settings within Infusionsoft', 'woocommerce'),
                'default' => ''
                ),
            'order_customtable' => array(
                'title' => __('Select Custom Fields for', 'woocommerce'),
                'type' => 'select',
                'description' => __('Custom Fields would be Set for Contacts or Orders , as selected.', 'woocommerce'),
                'options' => array(
                    '' => __('Please select...', 'woocommerce' ),
                    'Order' => __('Order', 'woocommerce' ),
                    'Contact' => __('Contact', 'woocommerce' )
                    )
                ),
            'order_customflds' => array(
                'title' => __('Custom Fields for above table', 'woocommerce'),
                'type' => 'text',
                'description' => __('woo_checkout_customFld1:_is_custom_fld1,woo_checkout_customFld1:_is_custom_fld2', 'woocommerce'),
                'default' => ''
                ),
            'order_product_customfld' => array(
                'title' => __('Custom Field Name for Product Ids in Order Table', 'woocommerce'),
                'type' => 'text',
                'description' => __('e.g _woois_product_custom_field, make sure its defined in Infusionsoft under Order Table', 'woocommerce'),
                'default' => ''
                ),
            'is_settings_tags' => array(
                'title' => __('Infusionsoft Tag Ids', 'woocommerce'),
                'type' => 'text',
                'description' => __('Comma separated infusionsoft tags(e.g 10,20,30).', 'woocommerce'),
                'default' => ''
                ),
            'is_merchant_id' => array(
                'title' => __('Infusionsoft Merchant ID', 'woocommerce'),
                'type' => 'text',
                'description' => __('Your Infusionsoft Merchant ID', 'woocommerce'),
                'default' => ''
                ),
            'title' => array(
                'title' => __('Title', 'woocommerce'),
                'type' => 'text',
                'description' => __('This controls the title which the user sees during checkout.', 'woocommerce'),
                'default' => __('Infusionsoft', 'woocommerce')
                ),
            'tax_label' => array(
                'title' => __('Tax Label', 'woocommerce'),
                'type' => 'text',
                'description' => __('Specify Tax Label based on your country(VAT/Sales Tax).', 'woocommerce'),
                'default' => __('Sales Tax', 'woocommerce')
                ),
            'is_free_shipping' => array(
                'title' => __('Free Shipping Item Should be Added?', 'woocommerce'),
                'type' => 'checkbox',
                'description' => __('When turned on the system will create a 0.00 amount free shipping order item in infusionsoft order', 'woocommerce'),
                'default' => 'no'
                ),            
            'description' => array(
                'title' => __('Description', 'woocommerce'),
                'type' => 'textarea',
                'description' => __('This controls the description which the user sees during checkout.', 'woocommerce'),
                'default' => __("Pay via Infusionsoft", 'woocommerce')
                ),
            'cards' => array(
                'title' => __('Accepted Card Types', 'woocommerce'),
                'type' => 'textarea',
                'description' => __('The lists of cards accepted by your Infusionsoft account. Add one entry per line. If left blank then Visa and Mastercard will be used', 'woocommerce'),
                'default' => __("VISA\nMASTERCARD", 'woocommerce')
                ),
            'wooorderstatus' => array(
                'title' => __('Mark WooComerce Order as..', 'woocommerce'),
                'type' => 'select',
                'description' => __('This status will be updated in WooCommerce after the InfusioSoft payment is charged.', 'woocommerce'),
                'options' => array(
                    '' => __('Please select...', 'woocommerce' ),
                    'pending' => __('Pending', 'woocommerce' ),
                    'processing' => __('Processing', 'woocommerce' ),
                    'on-hold' => __('On-hold', 'woocommerce' ),
                    'completed' => __('Completed', 'woocommerce' ),
                    'cancelled' => __('Cancelled', 'woocommerce' ),
                    'failed' => __('Failed', 'woocommerce' ),
                    'refunded' => __('Refunded', 'woocommerce' )
                    )
                ),            
            'thanks_message' => array(
                'title' => __('Thanks message', 'woocommerce'),
                'type' => 'textarea',
                'description' => __('The message to show on a successful payment', 'woocommerce'),
                'default' => __("Thank you. Your order has been received", 'woocommerce')
                ),
            'debug' => array(
                'title' => __('Debug', 'woocommerce'),
                'type' => 'checkbox',
                'label' => __('Enable logging (<code>woocommerce/logs/infusionsoft.txt</code>)', 'woocommerce'),
                'default' => 'no'
                ),
            'debug_email' => array(
                'title' => __('IPN Debug Email', 'woocommerce'),
                'type' => 'text',
                'label' => __('Email address to send IPN info to. Used for debugging. Blank for no email', 'woocommerce'),
                'default' => ''
                )           
            );
//error_log("init_form_fields-Ends=".date('Y-m-d H:i:s'));                                   
}

function validate_fields() {
//error_log("validate_fields-Starts=".date('Y-m-d H:i:s'));                                                         
    global $woocommerce;

    $return = true;
    $required = array(
        'is_ccnum' => 'Card Number'
        , 'is_card_type' => 'Card Type'
        , 'is_expmonth' => 'Expiry Month'
        , 'is_expyear' => 'Expiry Year'
        , 'is_cvv' => 'Card Security Code'
        );

    foreach ($required as $key => $value) {
        if (isset($_POST[$key])) {
            if ($_POST[$key] == '') {
                $error_message = 'Please enter your ' . $value;
                    //$woocommerce->add_error($error_message); //if there was an error
                wc_add_notice($error_message, "error");
                $return = false;
            }
        }
    }

        $return = apply_filters('wc_is_purchase_validate', $return); //for third party scripts to add their own validation to the checkout

        if ($return) { //second level validation
            $_POST['is_exp'] = mktime(0, 0, 0, $_POST['is_expmonth'], 1, $_POST['is_expyear']);
            if (time() >= $_POST['is_exp']) {
                $error_message = 'Please enter an Expiry Date in the future';
                //$woocommerce->add_error($error_message); //if there was an error
                wc_add_notice($error_message, "error");
                $return = false;
            }

            $cvv_length = strlen($_POST['is_cvv']);
            if ($cvv_length < 3 || $cvv_length > 4) {
                $error_message = 'Please enter a Valid Security Code';
                //$woocommerce->add_error($error_message); //if there was an error
                wc_add_notice($error_message, "error");
                $return = false;
            }

            $_POST['is_ccnum'] = trim(str_replace(' ', '', $_POST['is_ccnum'])); //remove any spacing from the card number
            if (!is_numeric($_POST['is_ccnum'])) {
                $error_message = 'Card Number should be 16 numbers with no spaces';
                //$woocommerce->add_error($error_message); //if there was an error
                wc_add_notice($error_message, "error");
                $return = false;
            } else if (strlen($_POST['is_ccnum']) < 15) {
                $error_message = 'Card Number should be at least 16 numbers (15 for AMEX) with no spaces';
                //$woocommerce->add_error($error_message); //if there was an error
                wc_add_notice($error_message, "error");
                $return = false;
            }
        }
//error_log("validate_fields-Ends=".date('Y-m-d H:i:s'));                                                         
        return $return;
    }

    function payment_fields() {
//error_log("payment_fields-Starts=".date('Y-m-d H:i:s'));                                           
        if ($this->description) {
            echo wpautop(wptexturize($this->description));
        }

        $cards = explode("\n", $this->cards);

        $year_options = '';
        for ($i = date('Y'); $i <= (date('Y') + 10); $i++) {
            $year_options .= '<option value="' . $i . '">' . $i . '</option>';
        }

        echo '	<fieldset>

        <p class="form-row form-row-first">
         <label for="is_ccnum">Card number <span class="required">*</span></label>
         <input type="text" class="input-text" id="is_ccnum" name="is_ccnum" />
     </p>

     <p class="form-row form-row-last">
         <label for="is_card_type">Card type <span class="required">*</span></label>
         <select name="is_card_type" id="is_card_type" class="woocommerce-select">
            <option value="">Select Card</option>';

            foreach ($cards as $card) {
                echo '<option value="' . trim($card) . '">' . trim($card) . '</option>';
            }

            echo '		</select>
        </p>

        <div class="clear"></div>

        <p class="form-row form-row-first">
          <label for="cc-expire-month">Expiration date <span class="required">*</span></label>
          <select name="is_expmonth" id="is_expmonth" class="woocommerce-select woocommerce-cc-month">
            <option value="">Month</option>
            <option value="01">January</option>
            <option value="02">February</option>
            <option value="03">March</option>
            <option value="04">April</option>
            <option value="05">May</option>
            <option value="06">June</option>
            <option value="07">July</option>
            <option value="08">August</option>
            <option value="09">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>
        </select>
        <select name="is_expyear" id="is_expyear" class="woocommerce-select woocommerce-cc-year">
          <option value="">Year</option>
          ' . $year_options . '
      </select>
  </p>

  <p class="form-row form-row-last">
     <label for="is_cvv">Card security Code <span class="required">*</span></label>
     <input type="text" class="input-text" id="is_cvv" name="is_cvv" maxlength="4" style="width:45px" />
     <span class="help">3 or 4 digits.</span>
 </p>

 <div class="clear"></div>
</fieldset>	';
//error_log("payment_fields-Ends=".date('Y-m-d H:i:s'));         
}

function get_failure_reason($id) {
    return (isset($this->failed_reasons[$id]) ? $this->failed_reasons[$id] : false);
}

function thankyou_page() {
    if ($this->debug == 'yes') {
        $this->log->add('infusionsoft', 'thankyou_page : feedback_message='.$this->feedback_message);
    }

    echo wpautop($this->feedback_message);
}

function infusionsoft_thanks() {
    echo $this->response_codes(urldecode($_GET['desc']));
}

function translate_country($woo_country) {
    $return = $woo_country;

    $countries_lookup = array(
        'AF'=> 'Afghanistan',
        'AX'=> 'Aland Islands',
        'AL'=> 'Albania',
        'DZ'=> 'Algeria',
        'AS'=> 'American Samoa',
        'AD'=> 'Andorra',
        'AO'=> 'Angola',
        'AI'=> 'Anguilla',
        'AQ'=> 'Antarctica',
        'AG'=> 'Antigua and Barbuda',
        'AR'=> 'Argentina',
        'AM'=> 'Armenia',
        'AW'=> 'Aruba',
        'AU'=> 'Australia',
        'AT'=> 'Austria',
        'AZ'=> 'Azerbaijan',
        'BS'=> 'Bahamas the',
        'BH'=> 'Bahrain',
        'BD'=> 'Bangladesh',
        'BB'=> 'Barbados',
        'BY'=> 'Belarus',
        'BE'=> 'Belgium',
        'BZ'=> 'Belize',
        'BJ'=> 'Benin',
        'BM'=> 'Bermuda',
        'BT'=> 'Bhutan',
        'BO'=> 'Bolivia',
        'BA'=> 'Bosnia and Herzegovina',
        'BW'=> 'Botswana',
        'BV'=> 'Bouvet Island (Bouvetoya)',
        'BR'=> 'Brazil',
        'IO'=> 'British Indian Ocean Territory (Chagos Archipelago)',
        'VG'=> 'British Virgin Islands',
        'BN'=> 'Brunei Darussalam',
        'BG'=> 'Bulgaria',
        'BF'=> 'Burkina Faso',
        'BI'=> 'Burundi',
        'KH'=> 'Cambodia',
        'CM'=> 'Cameroon',
        'CA'=> 'Canada',
        'CV'=> 'Cape Verde',
        'KY'=> 'Cayman Islands',
        'CF'=> 'Central African Republic',
        'TD'=> 'Chad',
        'CL'=> 'Chile',
        'CN'=> 'China',
        'CX'=> 'Christmas Island',
        'CC'=> 'Cocos (Keeling) Islands',
        'CO'=> 'Colombia',
        'KM'=> 'Comoros the',
        'CD'=> 'Congo',
        'CG'=> 'Congo the',
        'CK'=> 'Cook Islands',
        'CR'=> 'Costa Rica',
        'CI'=> 'Cote d\'Ivoire',
        'HR'=> 'Croatia',
        'CU'=> 'Cuba',
        'CY'=> 'Cyprus',
        'CZ'=> 'Czech Republic',
        'DK'=> 'Denmark',
        'DJ'=> 'Djibouti',
        'DM'=> 'Dominica',
        'DO'=> 'Dominican Republic',
        'EC'=> 'Ecuador',
        'EG'=> 'Egypt',
        'SV'=> 'El Salvador',
        'GQ'=> 'Equatorial Guinea',
        'ER'=> 'Eritrea',
        'EE'=> 'Estonia',
        'ET'=> 'Ethiopia',
        'FO'=> 'Faroe Islands',
        'FK'=> 'Falkland Islands (Malvinas)',
        'FJ'=> 'Fiji the Fiji Islands',
        'FI'=> 'Finland',
        'FR'=> 'France, French Republic',
        'GF'=> 'French Guiana',
        'PF'=> 'French Polynesia',
        'TF'=> 'French Southern Territories',
        'GA'=> 'Gabon',
        'GM'=> 'Gambia the',
        'GE'=> 'Georgia',
        'DE'=> 'Germany',
        'GH'=> 'Ghana',
        'GI'=> 'Gibraltar',
        'GR'=> 'Greece',
        'GL'=> 'Greenland',
        'GD'=> 'Grenada',
        'GP'=> 'Guadeloupe',
        'GU'=> 'Guam',
        'GT'=> 'Guatemala',
        'GG'=> 'Guernsey',
        'GN'=> 'Guinea',
        'GW'=> 'Guinea-Bissau',
        'GY'=> 'Guyana',
        'HT'=> 'Haiti',
        'HM'=> 'Heard Island and McDonald Islands',
        'VA'=> 'Holy See (Vatican City State)',
        'HN'=> 'Honduras',
        'HK'=> 'Hong Kong',
        'HU'=> 'Hungary',
        'IS'=> 'Iceland',
        'IN'=> 'India',
        'ID'=> 'Indonesia',
        'IR'=> 'Iran',
        'IQ'=> 'Iraq',
        'IE'=> 'Ireland',
        'IM'=> 'Isle of Man',
        'IL'=> 'Israel',
        'IT'=> 'Italy',
        'JM'=> 'Jamaica',
        'JP'=> 'Japan',
        'JE'=> 'Jersey',
        'JO'=> 'Jordan',
        'KZ'=> 'Kazakhstan',
        'KE'=> 'Kenya',
        'KI'=> 'Kiribati',
        'KP'=> 'Korea',
        'KR'=> 'Korea',
        'KW'=> 'Kuwait',
        'KG'=> 'Kyrgyz Republic',
        'LA'=> 'Lao',
        'LV'=> 'Latvia',
        'LB'=> 'Lebanon',
        'LS'=> 'Lesotho',
        'LR'=> 'Liberia',
        'LY'=> 'Libyan Arab Jamahiriya',
        'LI'=> 'Liechtenstein',
        'LT'=> 'Lithuania',
        'LU'=> 'Luxembourg',
        'MO'=> 'Macao',
        'MK'=> 'Macedonia',
        'MG'=> 'Madagascar',
        'MW'=> 'Malawi',
        'MY'=> 'Malaysia',
        'MV'=> 'Maldives',
        'ML'=> 'Mali',
        'MT'=> 'Malta',
        'MH'=> 'Marshall Islands',
        'MQ'=> 'Martinique',
        'MR'=> 'Mauritania',
        'MU'=> 'Mauritius',
        'YT'=> 'Mayotte',
        'MX'=> 'Mexico',
        'FM'=> 'Micronesia',
        'MD'=> 'Moldova',
        'MC'=> 'Monaco',
        'MN'=> 'Mongolia',
        'ME'=> 'Montenegro',
        'MS'=> 'Montserrat',
        'MA'=> 'Morocco',
        'MZ'=> 'Mozambique',
        'MM'=> 'Myanmar',
        'NA'=> 'Namibia',
        'NR'=> 'Nauru',
        'NP'=> 'Nepal',
        'AN'=> 'Netherlands Antilles',
        'NL'=> 'Netherlands the',
        'NC'=> 'New Caledonia',
        'NZ'=> 'New Zealand',
        'NI'=> 'Nicaragua',
        'NE'=> 'Niger',
        'NG'=> 'Nigeria',
        'NU'=> 'Niue',
        'NF'=> 'Norfolk Island',
        'MP'=> 'Northern Mariana Islands',
        'NO'=> 'Norway',
        'OM'=> 'Oman',
        'PK'=> 'Pakistan',
        'PW'=> 'Palau',
        'PS'=> 'Palestinian Territory',
        'PA'=> 'Panama',
        'PG'=> 'Papua New Guinea',
        'PY'=> 'Paraguay',
        'PE'=> 'Peru',
        'PH'=> 'Philippines',
        'PN'=> 'Pitcairn Islands',
        'PL'=> 'Poland',
        'PT'=> 'Portugal, Portuguese Republic',
        'PR'=> 'Puerto Rico',
        'QA'=> 'Qatar',
        'RE'=> 'Reunion',
        'RO'=> 'Romania',
        'RU'=> 'Russian Federation',
        'RW'=> 'Rwanda',
        'BL'=> 'Saint Barthelemy',
        'SH'=> 'Saint Helena',
        'KN'=> 'Saint Kitts and Nevis',
        'LC'=> 'Saint Lucia',
        'MF'=> 'Saint Martin',
        'PM'=> 'Saint Pierre and Miquelon',
        'VC'=> 'Saint Vincent and the Grenadines',
        'WS'=> 'Samoa',
        'SM'=> 'San Marino',
        'ST'=> 'Sao Tome and Principe',
        'SA'=> 'Saudi Arabia',
        'SN'=> 'Senegal',
        'RS'=> 'Serbia',
        'SC'=> 'Seychelles',
        'SL'=> 'Sierra Leone',
        'SG'=> 'Singapore',
        'SK'=> 'Slovakia (Slovak Republic)',
        'SI'=> 'Slovenia',
        'SB'=> 'Solomon Islands',
        'SO'=> 'Somalia, Somali Republic',
        'ZA'=> 'South Africa',
        'GS'=> 'South Georgia and the South Sandwich Islands',
        'ES'=> 'Spain',
        'LK'=> 'Sri Lanka',
        'SD'=> 'Sudan',
        'SR'=> 'Suriname',
        'SJ'=> 'Svalbard & Jan Mayen Islands',
        'SZ'=> 'Swaziland',
        'SE'=> 'Sweden',
        'CH'=> 'Switzerland, Swiss Confederation',
        'SY'=> 'Syrian Arab Republic',
        'TW'=> 'Taiwan',
        'TJ'=> 'Tajikistan',
        'TZ'=> 'Tanzania',
        'TH'=> 'Thailand',
        'TL'=> 'Timor-Leste',
        'TG'=> 'Togo',
        'TK'=> 'Tokelau',
        'TO'=> 'Tonga',
        'TT'=> 'Trinidad and Tobago',
        'TN'=> 'Tunisia',
        'TR'=> 'Turkey',
        'TM'=> 'Turkmenistan',
        'TC'=> 'Turks and Caicos Islands',
        'TV'=> 'Tuvalu',
        'UG'=> 'Uganda',
        'UA'=> 'Ukraine',
        'AE'=> 'United Arab Emirates',
        'GB'=> 'United Kingdom',
        'US'=> 'United States of America',
        'UM'=> 'United States Minor Outlying Islands',
        'VI'=> 'United States Virgin Islands',
        'UY'=> 'Uruguay, Eastern Republic of',
        'UZ'=> 'Uzbekistan',
        'VU'=> 'Vanuatu',
        'VE'=> 'Venezuela',
        'VN'=> 'Vietnam',
        'WF'=> 'Wallis and Futuna',
        'EH'=> 'Western Sahara',
        'YE'=> 'Yemen',
        'ZM'=> 'Zambia',
        'ZW'=> 'Zimbabwe'
        );

if (isset($countries_lookup[$return])) {
    $return = $countries_lookup[$return];
}

return $return;
}

function isWooCommerceCheckoutEmailOptInEnabled($woo_order){
//error_log("isWooCommerceCheckoutEmailOptInEnabled-Starts=".date('Y-m-d H:i:s'));                                   
    $my_order_meta = get_post_custom($woo_order->id);
    if ($this->debug == 'yes') {
        $this->log->add('infusionsoft', 'isWooCommerceCheckoutEmailOptInEnabled : my_order_meta='.print_r($my_order_meta, true));
    }

    $emailOptinCheckBoxValue = $my_order_meta['_is_email_optin'][0];
    if ($this->debug == 'yes') {
        $this->log->add('infusionsoft', 'isWooCommerceCheckoutEmailOptInEnabled : emailOptinCheckBoxValue='.$emailOptinCheckBoxValue);
    }
//error_log("isWooCommerceCheckoutEmailOptInEnabled-Ends=".date('Y-m-d H:i:s'));                                   
    return $emailOptinCheckBoxValue;
}

function processOrderCustomFields($infusionApp,$woo_order,$isContactID,$isOrderId, $orderCustomtable,$orderCustomFld){
//error_log("processOrderCustomFields-Starts=".date('Y-m-d H:i:s'));                 
    if ($this->debug == 'yes') {
        $this->log->add('infusionsoft', 'processOrderCustomFields : isContactID='.$isContactID.'   isOrderId=' . $isOrderId . ' orderCustomtable='.$orderCustomtable.'    orderCustomFld=' . $orderCustomFld);
    }
        //e.g billing_event_date:billing_custom_fld1,billing_hear_about_us:billing_custom_fld2
    if(strlen($orderCustomFld) > 0 && strlen($isOrderId) > 0){
        $my_order_meta = get_post_custom($woo_order->id);
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'processOrderCustomFields : woo_order my_order_meta=' . print_r($my_order_meta, true));
        }

            //Create Custom Fleds if Order Table, if not there
        $orderCustomFldArr = explode(",",$orderCustomFld);
        foreach($orderCustomFldArr as $orderCustomFldstr){
            if ($this->debug == 'yes') {
                $this->log->add('infusionsoft', 'processOrderCustomFields : orderCustomFldstr=' . $orderCustomFldstr);
            }
            $orderCustomFldstrArr = explode(":", $orderCustomFldstr);
            $orderWooFldName = $orderCustomFldstrArr[0];

            $orderWooFldValue = $my_order_meta[$orderWooFldName][0];

            // Get the expiration date of the coupon.
            if ($orderWooFldName == '_wcnoc_id') {
                $coupon_meta = get_post_custom($orderWooFldValue);
                $orderWooFldValue = date("l, F jS", strtotime($coupon_meta['expiry_date'][0]));
            }
            // End custom code

            $orderISCustomFldName = $orderCustomFldstrArr[1];                
            if ($this->debug == 'yes') {
                $this->log->add('infusionsoft', 'processOrderCustomFields : orderWooFldName=' . $orderWooFldName."     orderWooFldValue=".$orderWooFldValue."     orderISCustomFldName=".$orderISCustomFldName);
            }
            if(!empty($orderWooFldName)){
                    //Date Type Handling
                $actualOrderISCustomFldName= ltrim($orderISCustomFldName, "_");
                $qry = array('Name' => $actualOrderISCustomFldName);
                $rets = array('Id','DataType','FormId','GroupId','Name','Label', 'DefaultValue', 'Values', 'ListRows');
                $formFields = $infusionApp->dsQuery("DataFormField", 1, 0, $qry, $rets);
                $customFldType = $formFields[0]['DataType'];
                    if($customFldType == 13){//Date Type
                        $orderWooFldValue = $infusionApp->infuDate($orderWooFldValue);
                        if ($this->debug == 'yes') {
                            $this->log->add('infusionsoft', 'processOrderCustomFields : Date orderWooFldName=' . $orderWooFldName."     Date orderWooFldValue=".$orderWooFldValue."     orderISCustomFldName=".$orderISCustomFldName);
                        }
                    }
                    
                    $orderData = array($orderISCustomFldName => $orderWooFldValue);

                    if(strcasecmp($orderCustomtable, "Contact") == 0 ){
                     $contactUpdate = $infusionApp->dsUpdate("Contact", $isContactID, $orderData);
                     if ($this->debug == 'yes') {
                        $this->log->add('infusionsoft', 'processOrderCustomFields : contactUpdate=' . print_r($contactUpdate, true));
                    }
                }
                if(strcasecmp($orderCustomtable, "Order") == 0 ){
                 $orderUpdate = $infusionApp->dsUpdate("Job", $isOrderId, $orderData);
                 if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', 'processOrderCustomFields : orderUpdate=' . print_r($orderUpdate, true));
                }
            }

        }
                //Create the Order Custom Fld if doesnt exist
            }//End of forEach
        }//End of if
//error_log("processOrderCustomFields-Ends=".date('Y-m-d H:i:s'));                         
    }

    function process_import_add_tag($infusionApp, $table, $query, $return_fields=false, $insert_if_false=false) {
//error_log("process_import_add_tag-Starts=".date('Y-m-d H:i:s'));                                 
        if (!$return_fields) {
            $return_fields = array('Id');
        }

        $return = $infusionApp->dsQuery($table, 1, 0, $query, $return_fields);

        if (empty($return)) {
            if ($insert_if_false) {
                $return = $infusionApp->dsAdd($table, $query);
            }
        } else {
            if (is_array($return)) {
                $return = array_pop($return[0]);
            } else {
                $return = false; 
            }
        }
//error_log("process_import_add_tag-Ends=".date('Y-m-d H:i:s'));                                         
        return $return;
    }        
    
    function process_payment($order_id, $pre_payment = false, $pushToIs = false) {
//error_log("process_payment-Starts=".date('Y-m-d H:i:s'));                                                 
        global $woocommerce;
        

        /*//$checkoutFields = WC_Checkout::instance()->checkout()->checkout_fields;
        $checkout = new WC_Checkout();
        error_log(print_r($checkout->get_value('billing_country'), true));   */

        $order = new WC_Order($order_id);
        

        $items = $order->get_items();
        $creditCardID = NULL;
        $assign_goals = array();
        $assign_tags = array();
        $contactID = NULL;
        $is_aff_id = 0;
        $infusionApp = new c4wc_iSDK;
        $promoError = false;

        $response = $infusionApp->cfgCon($this->is_application_name, $this->is_api_key);

        if ($this->debug_email) {
            wp_mail($this->debug_email, 'Pre-Infusionsoft Payment Debug Feedback', print_r($infusionApp, true) . "\n" . print_r($items, true) . "\n" . print_r($_GET, true) . "\n" . print_r($_POST, true) . "\n" . print_r($_SESSION, true) . "\n" . print_r($order, true));
        }

        
        do_action('wc_is_pre_purchase', $infusionApp, $order_id); //hooks for other plugins

        $address_types = array('shipping', 'billing');
        $address_fields = array(
            'first_name'
            , 'email'
            , 'last_name'
            , 'company'
            , 'address_1'
            , 'address_2'
            , 'city'
            , 'state'
            , 'postcode'
            , 'country'
            , 'phone'
            );

        $address_data = array();
        foreach ($address_types as $address_type) {
            foreach ($address_fields as $address_field) {
                $class_field = $address_type . '_' . $address_field;
                $address_data[$class_field] = $order->$class_field;
            }
        }

        $contact = array();
        $contact['Email'] = trim($address_data['billing_email']);
        $contact['FirstName'] = trim($address_data['billing_first_name']);
        $contact['LastName'] = trim($address_data['billing_last_name']);
        $contact['StreetAddress1'] = trim($address_data['billing_address_1']);
        $contact['StreetAddress2'] = trim($address_data['billing_address_2']);
        $contact['City'] = trim($address_data['billing_city']);
        $contact['State'] = trim($address_data['billing_state']);
        $contact['PostalCode'] = trim($address_data['billing_postcode']);
        $contact['Country'] = trim($this->translate_country($address_data['billing_country']));
        $contact['Company'] = trim($address_data['billing_company']);
        $contact['Phone1'] = trim($address_data['billing_phone']);


        $ship_prefix = 'shipping_';
        if (@$_POST['shiptobilling'] == 1) {
            $ship_prefix = 'billing_';
        }

        $shipping_address = array();
        $shipping_address['ShipFirstName'] = trim($address_data[$ship_prefix . 'first_name']);
        $shipping_address['ShipLastName'] = trim($address_data[$ship_prefix . 'last_name']);
        $shipping_address['ShipCompany'] = trim($address_data[$ship_prefix . 'company']);
        $shipping_address['JobNotes'] = trim($order->customer_note);
        $shipping_address['ShipStreet1'] = trim($address_data[$ship_prefix . 'address_1']);
        $shipping_address['ShipStreet2'] = trim($address_data[$ship_prefix . 'address_2']);
        $shipping_address['ShipCity'] = trim($address_data[$ship_prefix . 'city']);
        $shipping_address['ShipState'] = trim($address_data[$ship_prefix . 'state']);
        $shipping_address['ShipZip'] = trim($address_data[$ship_prefix . 'postcode']);
        $shipping_address['ShipCountry'] = trim($this->translate_country($address_data[$ship_prefix . 'country']));

        if (!$shipping_address['ShipCity']) {
            $ship_prefix = 'billing_'; //if there is no shipping info then use billing.

            $shipping_address = array();
            $shipping_address['ShipFirstName'] = trim($address_data[$ship_prefix . 'first_name']);
            $shipping_address['ShipLastName'] = trim($address_data[$ship_prefix . 'last_name']);
            $shipping_address['ShipCompany'] = trim($address_data[$ship_prefix . 'company']);
            $shipping_address['JobNotes'] = trim($order->customer_note);
            $shipping_address['ShipStreet1'] = trim($address_data[$ship_prefix . 'address_1']);
            $shipping_address['ShipStreet2'] = trim($address_data[$ship_prefix . 'address_2']);
            $shipping_address['ShipCity'] = trim($address_data[$ship_prefix . 'city']);
            $shipping_address['ShipState'] = trim($address_data[$ship_prefix . 'state']);
            $shipping_address['ShipZip'] = trim($address_data[$ship_prefix . 'postcode']);
            $shipping_address['ShipCountry'] = trim($this->translate_country($address_data[$ship_prefix . 'country']));
        }

        //Add Shipping Address for the Contact
        $contact['Address2Street1'] = $shipping_address['ShipStreet1'];
        $contact['Address2Street2'] = $shipping_address['ShipStreet2'];
        $contact['City2'] = $shipping_address['ShipCity'];
        $contact['State2'] = $shipping_address['ShipState'];
        $contact['PostalCode2'] = $shipping_address['ShipZip'];
        $contact['Country2'] = $shipping_address['ShipCountry'];
        
        if (!$pre_payment) {
            $card = array();
            $card['CardType'] = trim($_POST['is_card_type']);
            $card['CardNumber'] = trim($_POST['is_ccnum']);
            $card['ExpirationMonth'] = trim($_POST['is_expmonth']);
            $card['ExpirationYear'] = trim($_POST['is_expyear']);
            //$card['StartDateMonth'] = trim($_POST['CardStartDateMonth']);
            //$card['StartDateYear'] = trim($_POST['CardStartDateYear']);
            $card['CVV2'] = trim($_POST['is_cvv']);
            $card['NameOnCard'] = $contact['FirstName'] . " " . $contact['LastName'];
            $card['FirstName'] = $contact['FirstName'];
            $card['LastName'] = $contact['LastName'];
            $card['BillAddress1'] = $contact['StreetAddress1'];
            $card['BillAddress2'] = $contact['StreetAddress2'];
            $card['BillCity'] = $contact['City'];
            $card['BillState'] = $contact['State'];
            //$card['BillCountry'] = $contact['Country'];
            $card['BillCountry'] = $address_data['billing_country'];
            //$card['BillZip'] = $address_data[$ship_prefix . 'country'];
            $card['BillZip'] = $contact['PostalCode'];
            $card['Email'] = $contact['Email']; 
        }

        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'Credit card Details' . print_r($card, true));
        }
        
        // Query the software to see if the contact email exists.
        $queryEmail = array('Email' => $contact['Email']);
        $returnFields = array('Id');
        $contactExists = $infusionApp->dsQuery("Contact", 1, 0, $queryEmail, $returnFields);

        //Add Tag for the Product After Import
        $importer_group_name = 'Imported by Connector4 Importer from ' . str_replace('www.', '', $_SERVER['SERVER_NAME']);
        $importer_tag_id = $this->process_import_add_tag($infusionApp,'ContactGroup', array('GroupName' => $importer_group_name), false, true);
        
        
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'Adding Tag for Connector4 Importer importer_tag_id#' . $importer_tag_id);
        }
        
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'ContactExists=  '.$contactExists.' Contact Details:'. print_r($contact, true));
        }
        
        // If the contact exists, we will set our contact ID 
        if (!empty($contactExists)) {
            $contactID = $contactExists[0]['Id'];  // Get the contact ID. 
            $response = $infusionApp->updateCon($contactID, $contact); // Update the contact details with the information from the form.
            if ($this->debug == 'yes') {
                $this->log->add('infusionsoft', 'Contact Updated=  '. print_r($response, true));
            }
            
        } else {
            if ($this->debug == 'yes') {
                $this->log->add('infusionsoft', 'Adding new Contact for Order #' . $order_id . '. ' . print_r($contact, true));
            }
            $contactID = $infusionApp->addCon($contact); // Add a new contact to the system.
        }
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'ContactId=  '. $contactID);
        }

        
        $importerTagResult = $infusionApp->grpAssign($contactID, $importer_tag_id);
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'Added Tag for Connector4 Importer importerTagResult =' . $importerTagResult);
        }
        
        $infusionApp->optIn($contact['Email']); //opt the user into email marketing

        if (!$pre_payment) {
            $card['ContactId'] = $contactID; // Set the card contact ID.

            $creditCardID = $infusionApp->dsAdd("CreditCard", $card); // Add the credit card data.
            $cardResult = $infusionApp->validateCard($creditCardID); // Validate the credit card.
            // If the card result was invalid, then report the problem.
            if ($cardResult['Valid'] == 'false') {
                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', 'Order failed. Card details not valid. Order #' . $order_id . '. ' . print_r($cardResult, true));
                }

                $reason = 'Card Details are not valid-'.$cardResult['Message'];
                //$woocommerce->add_error(__('Transaction Failed: ') . $reason); // Transaction Failed Notice on Checkout
                wc_add_notice(__('Transaction Failed: '), "error");
                //return false;
            }
        }

        // Setup a new order.
        $orderType_FINANCECHARGE = 6;
        $orderType_PRODUCT = 4;
        $orderType_SERVICE = 3;
        $orderType_SHIPPING = 1;
        $orderType_SPECIAL = 7;
        $orderType_TAX = 2;
        $orderType_UNKNOWN = 0;
        $orderType_UPSELL = 5;

        $is_aff_id = 0;

        if (isset($_SESSION['is_aff_id'])) {
            $queryAff = array('AffCode' => $_SESSION['is_aff_id']);
            $returnFields = array('Id');
            if ($getAff = $infusionApp->dsQuery("Affiliate", 1, 0, $queryAff, $returnFields)) {
                $is_aff_id = $getAff[0]['Id']; // Get the affiliate ID.
            }else{
                $is_aff_id = (int)$_SESSION['is_aff_id'];
            }
        }else{
            $is_aff_id = (int)get_post_meta(get_the_ID(), 'is_affiliate_id');
        }
//error_log('In process payment : is_aff_id='.$is_aff_id);        
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'In process payment : is_aff_id='.$is_aff_id);
        }
        
        
        //Get the Product Id and check for Subscription, prevent creating Blank Order
        $wc_sub_product_id = 0;
        if(count($items) == 1){
            foreach ($items as $i => $item) {
             $wc_sub_product_id = (int)$item['product_id'];
             break;
         }
         $wc_subtype = get_post_meta($wc_sub_product_id, 'is_subscription', true);
     }

     if($wc_subtype != 1){
        if (!$orderID = $infusionApp->blankOrder($contactID, "New Order #" . $order_id . " from " . site_url(), date('Ymd\TH:i:s'), 0, $is_aff_id)) {
            $msg = __('Unable to create order with Infusionsoft');
            if ($this->debug == 'yes') {
                $this->log->add('infusionsoft', $msg);
            }
                //$woocommerce->add_error($msg);
            wc_add_notice($msg, "error");
            return false;
        }

        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'WooCommerce Order #' . $order_id . ' is IS Order #' . $orderID);
        }

        $queryJob = array('Id' => $orderID);
        $returnFields = array('JobId', 'LeadAffiliateId', 'AffiliateId');
            if (!$getJob = $infusionApp->dsQuery("Invoice", 1, 0, $queryJob, $returnFields)) {// grab the invoice id to update
                $msg = __('Unable to get Invoice ID from Infusionsoft');
                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', $msg);
                }
                //$woocommerce->add_error($msg);
                wc_add_notice($msg, "error");
                return false;
            } else {
                $job_id = $getJob[0]['JobId']; // Get the contact ID.
            }

            //$woocommerce->add_error(print_r($getJob, true));
            //return false;			

            if ($this->debug == 'yes') {
                $this->log->add('infusionsoft', "##Shipping Address :".print_r($shipping_address, true));
            }
            
            if (!$infusionApp->dsUpdate("Job", $job_id, $shipping_address)) {
                $msg = __('Unable to update job with Infusionsoft');
                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', $msg);
                }
                //$woocommerce->add_error($msg);
                wc_add_notice($msg, "error");
                return false;
            }

            if ($this->debug == 'yes') {
                $this->log->add('infusionsoft', 'WooCommerce Order #' . $order_id . ' is IS Order #' . $orderID . '. Items ' . print_r($items, true));
            }
        }

        $productIdStr = "";
        foreach ($items as $i => $item) {
            $variation_id = $item['product_id'];
            $isVariation = false;

            if (isset($item['variation_id'])) {
                if ($item['variation_id']) {
                    $variation_id = $item['variation_id'];
                    $isVariation = true;
                    if ($this->debug == 'yes') {
                        $this->log->add('infusionsoft', 'variation_id=' . $variation_id."       isVariation=".$isVariation);
                    }
                    
                }
            }

            $product_obj = get_post($item['product_id']);
            if ($this->debug == 'yes') {
                $this->log->add('infusionsoft', 'product_obj=' . print_r($product_obj, true));
            }
            
            if($isVariation){
                $price = round(get_post_meta($variation_id, '_price', true), 2);
                $productID = get_post_meta($variation_id, 'is_product_id', true);
                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', 'Tiku--Variation Product WooCommerce Price =' . $price."      productId=".$productID);
                }
                
            }else{
                //Tiku, Actually Get the Price from WooCommerce LineTotal
                $price = round($order->get_item_subtotal($item), 2);
                $productID = get_post_meta($item['product_id'], 'is_product_id', true);
                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', 'Tiku---Single Product WooCommerce Price =' . $price."      productId=".$productID);
                }
            }

            if($isVariation){
                if (!$sku = get_post_meta($variation_id, '_sku', true)) {
                    $sku = $product_obj->post_name;
                    $variation_id = $product_obj->ID;
                    if ($this->debug == 'yes') {
                        $this->log->add('infusionsoft', 'Tiku--Variation SKU IS Product ID =' .$productID);
                    }
                }
                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', 'Tiku---SKU =' . $sku);
                }                
            }            
            //get the product ID from meta. If it doesn't exist then override so that it can be repopulated by the system
            if ($productID = get_post_meta($variation_id, 'is_product_id', true)) {
                $query = array('Id' => $productID);
                $returnFields = array('Id');
                $product_query = $infusionApp->dsQuery("Product", 1, 0, $query, $returnFields);

                if (empty($product_query)) {
                    $productID = 0;
                }
            }
            //end get id from meta
            
            if ($this->debug == 'yes') {
                $this->log->add('infusionsoft', 'Before creating Product in IS ProductID =' . $productID."      SKU=".$sku);
            }
            
            if (!$productID) {

                $query = array('ProductName' => $product_obj->post_title);
                $returnFields = array('Id');
                $product_query = $infusionApp->dsQuery("Product", 1, 0, $query, $returnFields);

                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', 'Before creating Product in IS product_query =' . print_r($product_query, true));
                }
                
                if (empty($product_query) || $isVariation) { //fall back to sku
                    if(!empty($sku)){
                        $query = array('Sku' => $sku);
                        $returnFields = array('Id');
                        $product_query = $infusionApp->dsQuery("Product", 1, 0, $query, $returnFields);
                    }

                    if ($this->debug == 'yes') {
                        $this->log->add('infusionsoft', 'Before creating Product in IS product_query With SKU=' . print_r($product_query, true));
                    }
                    
                    if (empty($product_query)) {
                        $product = array(
                            'ProductName' => $product_obj->post_title
                            , 'Sku' => $sku
                            , 'Status' => (int) 1
                            , 'ShortDescription' => $product_obj->post_excerpt
                            , 'Description' => $product_obj->post_content
                            , 'ProductPrice' => $price
                            );
                        $productID = $infusionApp->dsAdd("Product", $product);

                        
                        if ($this->debug == 'yes') {
                            $this->log->add('infusionsoft', 'WooCommerce Order #' . $order_id . '. Added product to DB. Added Product ' . $productID);
                        }
                        if(!$isVariation)
                            update_post_meta($product_obj->ID, 'is_product_id', $productID);
                    } else {
                        $productID = $product_query[0]['Id'];
                        if ($this->debug == 'yes') {
                            $this->log->add('infusionsoft', 'Tiku---Variation prodictId =' . $productID);
                        }                
                    }
                } else {
                    if ($productID = $product_query[0]['Id']) {
                        update_post_meta($product_obj->ID, 'is_product_id', $productID);
                    }
                }
            } else {
                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', 'WooCommerce Order #' . $order_id . ' is IS Order #' . $orderID . '. Added item by product meta ' . $productID);
                }
            }

            if ($this->debug == 'yes') {
                $this->log->add('infusionsoft', 'WooCommerce Order #' . $order_id . ' is IS Order #' . $orderID . '. Added item ' . $productID);
            }
            
            $promoCode = get_post_meta($productID, 'is_promo_code', true);
            if(strlen($promoCode) <= 0)
                $promoCode = get_post_meta($product_obj->ID, 'is_promo_code', true);
            
            if ($this->debug == 'yes') {
                $this->log->add('infusionsoft', 'productId #' . $productID . ' promoCode=' . $promoCode);
            }
            
            if ($this->debug == 'yes') {
                $this->log->add('infusionsoft', 'is_subscription= ' . get_post_meta($item['product_id'], 'is_subscription', true));
            }            
            if (get_post_meta($item['product_id'], 'is_subscription', true) == 1) {
                $is_settings = get_option('woocommerce_infusionsoft_settings');
                $subapp = new c4wc_iSDK;
                $subapp->cfgCon($is_settings['is_application_name'], $is_settings['is_api_key']);
                $isProductID = get_post_meta($item['product_id'], 'is_product_id', true);
                
                $returnFields = array('Id', 'Cycle', 'Frequency', 'PreAuthorizeAmount', 'Prorate', 'Active', 'PlanPrice');
                $query = array('ProductId' => $isProductID,);
                $details = $subapp->dsQuery("SubscriptionPlan", 1, 0, $query, $returnFields);
                if (!$creditCardID) {
                    $creditCardID = 0;
                }
                if ($details) {

                    $carray = array(
                        php_xmlrpc_encode($subapp->key),
                        php_xmlrpc_encode((int) $contactID),
                        php_xmlrpc_encode((int) $creditCardID),
                        php_xmlrpc_encode(0),
                        php_xmlrpc_encode(array()),
                        php_xmlrpc_encode(array($details[0]['Id'])),
                        php_xmlrpc_encode(TRUE),
                        php_xmlrpc_encode(array(get_post_meta($item['product_id'], 'is_promo_code', true))), // array of strings
                        php_xmlrpc_encode(0),
                        php_xmlrpc_encode($is_aff_id)
                        );

                    $result = $subapp->methodCaller("OrderService.placeOrder", $carray);
                    $orderID = $result['InvoiceId'];
                    if ($this->debug == 'yes') {
                        $this->log->add('infusionsoft', 'WooCommerce Order #' . $order_id . ' is IS Place Order #' . $orderID);
                    }

                    $queryJob = array('Id' => $orderID);
                    $returnFields = array('JobId', 'LeadAffiliateId', 'AffiliateId');
                    if (!$getJob = $infusionApp->dsQuery("Invoice", 1, 0, $queryJob, $returnFields)) {// grab the invoice id to update
                        $msg = __('Unable to get Invoice ID from Infusionsoft');
                        if ($this->debug == 'yes') {
                            $this->log->add('infusionsoft', $msg);
                        }
                        //$woocommerce->add_error($msg);
                        wc_add_notice($msg, "error");
                        return false;
                    } else {
                        $job_id = $getJob[0]['JobId']; // Get the contact ID.
                    }

                    //$woocommerce->add_error(print_r($getJob, true));
                    //return false;			

                    if (!$infusionApp->dsUpdate("Job", $job_id, $shipping_address)) {
                        $msg = __('Unable to update job with Infusionsoft');
                        if ($this->debug == 'yes') {
                            $this->log->add('infusionsoft', $msg);
                        }
                        //$woocommerce->add_error($msg);
                        wc_add_notice($msg, "error");
                        return false;
                    }

                    if ($this->debug == 'yes') {
                        $this->log->add('infusionsoft', 'WooCommerce Order #' . $order_id . ' is IS Place Order #' . $orderID . '. Place Order result ' . print_r($result, true));
                    }                    
                    $updateData = array('JobTitle' => " New Order #$order_id from " . get_site_url());
                    if ($this->debug == 'yes') {
                        $this->log->add('infusionsoft', 'Subscription Product #' . print_r($details, true));
                    }
                    
                    /* Tiku-Add Tagid to ContactID Starts */
                    $tag_id = get_post_meta($item['product_id'], 'is_product_tag', true);
                    //$infusionApp->grpAssign($contactID, $tag_id);
                    /* Tiku-Add Tagid to ContactID Ends */

                    $update = $subapp->dsUpdate("Job", $orderID, $updateData);
                    $promoError = true;
                    /* return array(
                      'result' => 'success',
                      'redirect' => $this->get_return_url($order)
                      ); */
}
} else {
                // Add products to the order.
    if ($this->debug == 'yes') {
        $this->log->add('infusionsoft', 'IS AddOrderItem orderID=' . $orderID . ' productID' . $productID . '   price ' . $price.'      PostTitle='.$product_obj->post_title);
    }

    if (!$item_row = $infusionApp->addOrderItem($orderID, (int) $productID, $orderType_PRODUCT, (float) $price, (int) $item['qty'], $product_obj->post_title.(strlen($sku)>0 ? '(SKU='.$sku.')' : ''), $product_obj->post_title)) {
        $msg = __('Failed to add a product to the order: ') . $product_obj->post_title;
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', $msg);
        }
                    //$woocommerce->add_error($msg);
        wc_add_notice($msg, "error");
                    //return false;
    } else {
                    //achieve goal
                    $callName = 'product' . $productID . 'Sold'; // product123sold
                    //$infusionApp->achieveGoal($this->is_application_name, $callName, $contactID);

                    $assign_goals[$callName] = $contactID;

                    //apply tag per order item
                    if ($tag_id = get_post_meta($item['product_id'], 'is_product_tag', true)) {
                        $assign_tags[$tag_id] = $contactID;
                        //$infusionApp->grpAssign($contactID, $tag_id);
                    }
                }
            }
            $productIdStr .= $productID.",";
        }//End of for
        
        $productIdStr = rtrim($productIdStr, ",");
        if ((float) $order->order_tax) {
            //$vat_amount = ((float) $order->order_tax + (float) $order->order_shipping_tax);
            $vat_amount = wc_round_tax_total($order->order_tax) + wc_round_tax_total($order->order_shipping_tax);
            if (!$item_row = $infusionApp->addOrderItem($orderID, 0, $orderType_TAX, $vat_amount, (int) 1, strlen($this->tax_label) > 0 ? $this->tax_label : 'Sales Tax', strlen($this->tax_label) > 0 ? $this->tax_label : 'Sales Tax')) {
                $msg = __('Failed to add the tax product to the order. Tax amount: ') . $vat_amount;
                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', $msg);
                }
                //$woocommerce->add_error($msg);
                wc_add_notice($msg, "error");
                return false;
            }
        }

        if ((float) $order->order_shipping || $this->is_free_shipping == 'yes')  {
            //$shipping = (float) $order->order_shipping;
            $shipping = round($order->order_shipping, 2);
            if (!$item_row = $infusionApp->addOrderItem($orderID, 0, $orderType_SHIPPING, $shipping, (int) 1, "Shipping", "Shipping")) {
                $msg = __('Failed to add the shipping product to the order. Shipping amount: ') . $shipping;
                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', $msg);
                }
                //$woocommerce->add_error($msg);
                wc_add_notice($msg, "error");
                return false;
            }
        }
        //Create an ADD a Tag based on Shipping Method
        $shipping_method = $order->get_shipping_method();
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'Shipping Method for OrderId: #'.$order_id.'       ='.$shipping_method);
        }
        
        $this->addTagsForShippingDetails($infusionApp, $contactID, $shipping_method);
        //End of ADD a Tag based on Shipping Method
        
        if ((float) $order->get_total_discount()) {

            //$discount = (float) $order->get_total_discount();
            $discount = round($order->get_total_discount(), 2);
            $discount *= -1; //make negative

            if (!$item_row = $infusionApp->addOrderItem($orderID, 0, 11, $discount, (int) 1, "Discount", "Discount")) {
                $msg = __('Failed to add the discount product to the order. Discount amount: ') . $discount;
                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', $msg);
                }
                //$woocommerce->add_error($msg);
                wc_add_notice($msg, "error");
                return false;
            }
        }

        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'Starting processOrderCustomFields IS OrderId' . $orderID . ' order_customFlds from Settings:' .$this->order_customflds);
        }
        
        $job_id = $this->get_orderid_from_invoiceid($infusionApp, $orderID);
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'job_id='.$job_id.'     isOrderId=' . $orderID);
        }
        
        if(!empty($this->order_customtable) && !empty($this->order_customflds)){
            //Add Order Custom Flds
            $this->processOrderCustomFields($infusionApp,$order,$contactID,$job_id, $this->order_customtable,$this->order_customflds);
        }
        
        //Process order_product_customfld, concatenated Product ids
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'process order_product_customfld :order_product_customfld='.$this->order_product_customfld.'     productIdStr='.$productIdStr.'   isOrderId=' . $orderID);
        }
        if(strlen($this->order_product_customfld) > 0){
            $orderData = array($this->order_product_customfld => $productIdStr);
            $orderUpdate = $infusionApp->dsUpdate("Job", $job_id, $orderData);
            if ($this->debug == 'yes') {
               $this->log->add('infusionsoft', 'process order_product_customfld : orderUpdate=' . print_r($orderUpdate, true));
           }
       }
        //End of Process order_product_customfld, concatenated Product ids

        //Process ProductOptions CustomField
       $is_product_option_ids = get_post_meta($product_obj->ID, 'is_product_option_ids', true);
       if ($this->debug == 'yes') {
        $this->log->add('infusionsoft', 'Before clling processProductOptionsCustomField product_obj->ID ='.$product_obj->ID.'  is_product_option_ids=' . $is_product_option_ids);
    }        
    if(strlen($is_product_option_ids) > 0)
        $this->processProductOptionsCustomField($infusionApp,$job_id,$is_product_option_ids);

    if(strlen($this->thanks_message) > 0 && !$pushToIs)
        wc_add_notice($this->thanks_message);
            //WC_Admin_Settings::add_message($this->thanks_message);
        if (empty($promoCode)) {//with a promo code there is no balance due - so the charge is done automatically by IS later
            $amountOwed = $infusionApp->amtOwed($orderID);
            if (!$amountOwed = $infusionApp->amtOwed($orderID)) { // Calculate the amount owed for this order.
                $msg = __('Infusionsoft amount owed returned zero for order: ') . $orderID;
                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', $msg);
                }
                //WC_Admin_Settings::add_error($msg);
                //wc_add_notice($msg, "error");
                return false;
            } else {
                if ($amountOwed < 0) {
                    $msg = _e('Infusionsoft amount owed returned zero (' . $amountOwed . ') for order: ' . $orderID);
                    if ($this->debug == 'yes') {
                        $this->log->add('infusionsoft', $msg);
                    }
                    /*$woocommerce->add_error($msg);
                    return false;*/
                }
            }
        }

        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'Tik Testing Testing Testing');
        }
        
        
        do_action('wc_is_post_purchase', $infusionApp, $order_id, $contactID, $productID, $orderID, $job_id); //hooks for other plugins
        
        update_post_meta((int) $order_id, 'IS amount owed', $amountOwed);
        update_post_meta((int) $order_id, 'IS Order (Job) ID', $job_id);
        update_post_meta((int) $order_id, 'IS Invoice ID', $orderID);
        update_post_meta((int) $order_id, 'Payment Method', $order->payment_method_title);
        $order->add_order_note(__('IS Order ID', 'woocommerce') . ' - ' . $orderID);
        $order->add_order_note(__('Payment Method', 'woocommerce') . ' - ' . $this->method_title);
        if ($this->test_mode == 'yes') {
            //WC_Admin_Settings::add_error('At this point you have passed all forms of verification and your order should have been added to IS with the ID of ' . $orderID . ' (Job: ' . $job_id . ') for the value of ' . $amountOwed . '. Aff ID is: ' . $is_aff_id . '. Session Aff ID is: ' . (int) $_SESSION['is_aff_id'] . '. Take the IS module out of test mode to make real transactions.');
            wc_add_notice('At this point you have passed all forms of verification and your order should have been added to IS with the ID of ' . $orderID . ' (Job: ' . $job_id . ') for the value of ' . $amountOwed . '. Aff ID is: ' . $is_aff_id . '. Session Aff ID is: ' . (int) $_SESSION['is_aff_id'] . '. Take the IS module out of test mode to make real transactions.',
                "error");
            return false;
        }

        //if (get_post_meta($item['product_id'], 'is_subscription', true) != 1){
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'promoCode =' . $promoCode."       pre_payment=".$pre_payment);
        }
        
        if(empty($promoCode)){
            if (!$pre_payment) {
                if ($this->is_merchant_id) {
                    $paymentResult = $infusionApp->chargeInvoice($orderID, get_bloginfo('name') . " Payment", $creditCardID, $this->is_merchant_id, false); // Charge the Credit Card.
                    if ($this->debug == 'yes') {
                        $this->log->add('infusionsoft', 'ChargeInvoice for Woo Order' . $order_id . ' and  IS Order #' . $orderID . ': ' . print_r($paymentResult, true));
                    }
                    
                } else {
                    //WC_Admin_Settings::add_error(__('You have not added your IS Merchant ID. Add it to the IS settings page within WooCommerce and this gateway will function properly.'));
                    wc_add_notice(__('You have not added your IS Merchant ID. Add it to the IS settings page within WooCommerce and this gateway will function properly.'),
                        'error');
                    return false;
                }

                update_post_meta((int) $order_id, 'IS Ref Num', $paymentResult['RefNum']);

                // If we have failed, report the reason.
                if (!$paymentResult['Successful']) {
                    if ($this->debug == 'yes') {
                        $this->log->add('infusionsoft', 'WooCommerce Order #' . $order_id . ' is IS Order #' . $orderID . '. Payment failure: ' . print_r($paymentResult, true));
                    }

                    if ($this->debug_email) {
                        wp_mail($this->debug_email, 'Post-Infusionsoft Payment Debug Feedback - Payment FAILED', print_r($paymentResult, true) . "\n" . print_r($_POST, true) . "\n" . print_r($_SESSION, true) . "\n" . print_r($order, true));
                    }

                    //WC_Admin_Settings::add_error(__('Transaction Failed: ') . $paymentResult['Message']); // Transaction Failed Notice on Checkout
                    wc_add_notice(__('Transaction Failed: ') . $paymentResult['Message'], "error");
                    return;
                }

                //From here the payment has been successful

                if ($this->debug_email) {
                    wp_mail($this->debug_email, 'Post-Infusionsoft Payment Debug Feedback', print_r($paymentResult, true) . "\n" . print_r($items, true) . "\n" . print_r($_POST, true) . "\n" . print_r($_SESSION, true) . "\n" . print_r($order, true));
                }
            } else {
                //mark the invoice as paid
                $payDate = $infusionApp->infuDate(date('d-m-Y'));
                $manualPmtResult = $infusionApp->manualPmt($orderID, $amountOwed, $payDate, $order->payment_method_title, $order->payment_method_title, false);
                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', 'Manual Payment Done =' . print_r($manualPmtResult, true));
                }
            }
        }

        //assign goal
        foreach ($assign_goals as $goal_call_name => $goal_contact_id) {
            $infusionApp->achieveGoal($this->is_application_name, $goal_call_name, $goal_contact_id);
        }

        //apply tags
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'assign_tags' . print_r($assign_tags, true));
        }
        foreach ($assign_tags as $assign_tag_id => $assign_tag_contact_id) {
            $infusionApp->grpAssign($assign_tag_contact_id, $assign_tag_id);
        }
        
        
        //Apply Tags-Optin Feature
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'Apply Tags-Optin Feature for ContactID' . $contactID . ' = '.$this->is_settings_tags);
        }

        if($this->isWooCommerceCheckoutEmailOptInEnabled($order)){
            $is_settings_tagsArr = explode(",", $this->is_settings_tags);
            foreach($is_settings_tagsArr as $is_settings_tag){
                $optInTagResult = $infusionApp->grpAssign($contactID, $is_settings_tag);
                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', 'Tag '.$is_settings_tag.' is applied for contactID' . $contactID . ' = '.print_r($optInTagResult, true));
                }
            }
        }

        //foreach ($transaction as $k=>$v) {
        //update_post_meta((int)$order_id, $k, $v);
        //}
        foreach ($paymentResult as $k => $v) {
            update_post_meta((int) $order_id, $k, $v);
        }

        // Check order not already completed
        if (in_array($order->status, array('processing', 'completed'))) {
            if ($this->debug == 'yes') {
                $this->log->add('infusionsoft', 'Aborting, Order #' . $order_id . ' is already complete.');
            }
            return false;
        }

        //Make WooOrderStatus update for the Order
        $wooOrderStatus = strlen($this->wooorderstatus) > 0 ?  $this->wooorderstatus : 'completed';
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'wooOrderStatus='.$wooOrderStatus);
        }
        $order->update_status($wooOrderStatus, __( $wooOrderStatus, 'woocommerce' ));
        if ($this->debug == 'yes') {
            $this->log->add('infusionsoft', 'wooOrderStatus after update_status ' . print_r($order, true));
        }    
        
        if (!$pre_payment) {
            $order->add_order_note(__('Payment accepted - Order Successful', 'woocommerce'));
            $order->payment_complete();
            $woocommerce->cart->empty_cart();
        }

        $this->feedback_message = $this->thanks_message;
//error_log("process_payment-Ends=".date('Y-m-d H:i:s'));                                                 
        return array(
            'result' => 'success',
            'redirect' => $this->get_return_url($order)
            );
        
    }
    
    function processProductOptionsCustomField($infusionApp,$orderId,$is_product_option_ids){
      if ($this->debug == 'yes') {
          $this->log->add('infusionsoft', 'processProductOptionsCustomField is_product_option_ids=' . $is_product_option_ids);
      }        
      $is_product_option_idsArr = explode(":",$is_product_option_ids);
      $is_product_option_text_str = "";
      foreach($is_product_option_idsArr as $is_product_option_ids_each){
          if ($this->debug == 'yes') {
           $this->log->add('infusionsoft', 'processProductOptionsCustomField is_product_option_ids_each=' . $is_product_option_ids_each);
       }           
       $is_product_option_ids_eachArr = explode(",",$is_product_option_ids_each);
       $ProductOptionId = (int)$is_product_option_ids_eachArr[0];
       $ProductOptValueId = (int)$is_product_option_ids_eachArr[1];
       if ($this->debug == 'yes') {
           $this->log->add('infusionsoft', 'processProductOptionsCustomField ProductOptionId=' . $ProductOptionId."      ProductOptValueId=".$ProductOptValueId);
       }           
       if($ProductOptionId){
        $qry = array('Id' => $ProductOptionId);
        $rets = array('Id', 'ProductId','Name','Label', 'TextMessage','OptionType');
        $productOptions = $infusionApp->dsQuery("ProductOption", 1, 0, $qry, $rets); 
        if ($this->debug == 'yes') {
         $this->log->add('infusionsoft', 'processProductOptionsCustomField productOptions=' . print_r($productOptions, true));
     }                
     if(is_array($productOptions) && count($productOptions) > 0){
        $productOptionName = strlen($productOptions[0]['Label']) > 0 ? $productOptions[0]['Label'] : $productOptions[0]['Name'];
        if($ProductOptValueId){
            $qry = array('Id' => $ProductOptValueId);
            $rets = array('Id','Label', 'Sku','Name', 'IsDefault','OptionIndex','PriceAdjustment');
            $productOptionValues = $infusionApp->dsQuery("ProductOptValue", 1, 0, $qry, $rets);
            if ($this->debug == 'yes') {
             $this->log->add('infusionsoft', 'processProductOptionsCustomField productOptionValues=' . print_r($productOptionValues, true));
         }                

         if(is_array($productOptionValues) && count($productOptionValues) > 0){
            $productOptionValueName = strlen($productOptionValues[0]['Label']) > 0 ? $productOptionValues[0]['Label'] : $productOptionValues[0]['Name'];
        }
    } 
    $is_product_option_text_str .= $productOptionName.":".$productOptionValueName."     ;";
    if ($this->debug == 'yes') {
        $this->log->add('infusionsoft', 'processProductOptionsCustomField is_product_option_text_str=' . $is_product_option_text_str);
    }                    
}
          }//End of If $ProductOptionId
      }
      if(strlen($is_product_option_text_str) > 0){
        $orderData = array("_WooProductOptions" => $is_product_option_text_str);
        $orderUpdate = $infusionApp->dsUpdate("Job", $orderId, $orderData);
        if ($this->debug == 'yes') {
           $this->log->add('infusionsoft', 'processProductOptionsCustomField : orderUpdate=' . print_r($orderUpdate, true));
       }
   }
}

function get_orderid_from_invoiceid($infusionApp, $invoice_id) {
//error_log("get_orderid_from_invoiceid-Starts=".date('Y-m-d H:i:s'));                   
    global $woocommerce;
    $queryJob = array('Id' => $invoice_id);
    $returnFields = array('JobId');
        if (!$getJob = $infusionApp->dsQuery("Invoice", 1, 0, $queryJob, $returnFields)) {// grab the invoice id to update
            $msg = 'Unable to get Invoice ID from Infusionsoft';
            if ($this->debug == 'yes') {
                $this->log->add('infusionsoft', $msg);
            }
                //WC_Admin_Settings::add_error($msg);
            wc_add_notice($msg,"error");
            return $invoice_id;
        } else {
                $job_id = $getJob[0]['JobId']; // Get the Job  ID.
            }
//error_log("get_orderid_from_invoiceid-Ends=".date('Y-m-d H:i:s'));                   
            return $job_id;
        }    

        function addTagsForShippingDetails($app, $contactID, $shipping_method){
//error_log("addTagsForShippingDetails-Start=".date('Y-m-d H:i:s'));                       
            $tag_id = 0;
            if(strlen($shipping_method) > 0){
                $group_name = 'Woocommerce-Infusionsoft Module With Shipping Method:' . $shipping_method;
            //$tag_id = is_query_single('ContactGroup', array('GroupName' => $group_name), false, true);
                $query = array('GroupName' => $group_name);
                $return_fields = array('Id');
                $return = $app->dsQuery("ContactGroup", 1, 0, $query, $return_fields);

                if (empty($return)) {
                    $tag_id = $app->dsAdd("ContactGroup", $query);
                } else {
                    if (is_array($return)) {
                        $tag_id = array_pop($return[0]);
                    }
                }
                if ($this->debug == 'yes') {
                    $this->log->add('infusionsoft', 'addTagsForShippingDetails shipping_tag_id=' . $tag_id . ' $contactID' . $contactID);
                }
                if($tag_id){
                    $result = $app->grpAssign($contactID, $tag_id);
                    if ($this->debug == 'yes') {
                        $this->log->add('infusionsoft', 'addTagsForShippingDetails Added Tag=' . $result);
                    }
                }
            }
//error_log("addTagsForShippingDetails-Ends=".date('Y-m-d H:i:s'));                        
    }//End of Method
    
}

class WC_Gateway_SB_Test extends WC_Payment_Gateway {

    function __construct() {
        $this->id = 'sbtm';
        $this->icon = apply_filters('woocommerce_sbtm_icon', '');
        $this->method_title = __('SB Test Mode', 'woocommerce');
        $this->has_fields = false;

        // Load the settings
        $this->init_form_fields();
        $this->init_settings();

        $this->title = 'SB Test Mode';

        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
    }

    function admin_options() {
        ?>
        <h3><?php __('SB Test Mode', 'woocommerce'); ?></h3>
        <p><?php __('Simply a payment gateway to pretend to immediately take the user to a paid screen. IE bypasses any form of payment but marks the order as paid. Used for testing.', 'woocommerce'); ?></p>
        <table class="form-table">
            <?php $this->generate_settings_html(); ?>
        </table> <?php
    }

    function init_form_fields() {
        global $woocommerce;

        $this->form_fields = array(
            'enabled' => array(
                'title' => __('Enable SB Test Mode', 'woocommerce'),
                'label' => __('Enable SB Test Mode', 'woocommerce'),
                'type' => 'checkbox',
                'description' => '',
                'default' => 'no'
                )
            );
    }

    function is_available() {
        return true;
    }

    function process_payment($order_id) {
        global $woocommerce;

        $order = new WC_Order($order_id);
        $order->payment_complete();

        // Remove cart
        $woocommerce->cart->empty_cart();

        // Return thankyou redirect
        return array(
            'result' => 'success',
            'redirect' => add_query_arg('key', $order->order_key, add_query_arg('order', $order_id, get_permalink(woocommerce_get_page_id('thanks'))))
            );
    }

}

/**
 * Add the gateway to WooCommerce
 * */
function sb_add_infusionsoft_gateway($methods) {
    $methods[] = 'WC_Infusionsoft';
    //$methods[] = 'WC_Gateway_SB_Test'; //for testing payments
    return $methods;
}

add_filter('woocommerce_payment_gateways', 'sb_add_infusionsoft_gateway');
add_filter('woocommerce_payment_complete', 'sb_woocommerce_payment_complete');

function sb_woocommerce_payment_complete($order_id) {
//error_log("sb_woocommerce_payment_complete-Starts=".date('Y-m-d H:i:s'));      
//error_log("IS REF NUM=".get_post_meta($order_id, 'IS Ref Num', true));
    if (!get_post_meta($order_id, 'IS Ref Num', true)) { //we need to make sure that we don't run this
    $wc_is = new WC_Infusionsoft();
    $wc_is->process_payment($order_id, true);
}
//error_log("sb_woocommerce_payment_complete-Ends=".date('Y-m-d H:i:s'));                            
return $result;
}

/*
 * Filters infusionsoft only for subscription type
 */
add_filter('woocommerce_available_payment_gateways', 'filter_gateways', 1);

function filter_gateways($available_gateways) {
//error_log("filter_gateways-Starts=".date('Y-m-d H:i:s'));    
    global $woocommerce;
    $arrayKeys = array_keys($available_gateways);

    if (count($woocommerce->cart)) {
        $items = $woocommerce->cart->cart_contents;
        $itemsPays = '';
        if (is_array($items)) {
            foreach ($items as $item) {
                $itemsPays = array('infusionsoft');
                $subtype = get_post_meta($item['product_id'], 'is_subscription', true);

                if ($subtype == 1) {
                    foreach ($arrayKeys as $key) {
                        if (!in_array($available_gateways[$key]->id, $itemsPays)) {
                            unset($available_gateways[$key]);
                        }
                    }
                }
            }
        }
    }
//error_log("filter_gateways-Ends=".date('Y-m-d H:i:s'));        
    return $available_gateways;
}

add_filter('add_to_cart_redirect', 'redirect_to_checkout');

function redirect_to_checkout() {
//error_log("redirect_to_checkout-Starts=".date('Y-m-d H:i:s'));                        
    global $woocommerce;
    //Get product ID
    $product_id = (int) $_POST['add-to-cart'];

    //Check if product ID is in a certain taxonomy
    $subtype = get_post_meta($product_id, 'is_subscription', true);
    if ($subtype == 1) {
        //Get cart URL
        $checkout_url = get_permalink(get_option('woocommerce_checkout_page_id'));
        //Return the new URL
        return $checkout_url;
    }
//error_log("redirect_to_checkout-Ends=".date('Y-m-d H:i:s'));                            
}

add_action('admin_footer-edit.php', 'isl_woois_custom_bulk_admin_footer');

function isl_woois_custom_bulk_admin_footer() {
 //error_log("isl_woois_custom_bulk_admin_footer-Starts=".date('Y-m-d H:i:s'));        
  global $post_type;

  if($post_type == 'shop_order') {
    ?>
    <script type="text/javascript">
      jQuery(document).ready(function() {
        jQuery('<option>').val('potis').text('<?php _e('Push Order to Infusionsoft')?>').appendTo("select[name='action']");
        jQuery('<option>').val('potis').text('<?php _e('Push Order to Infusionsoft')?>').appendTo("select[name='action2']");
    });
  </script>
  <?php
}
  //error_log("isl_woois_custom_bulk_admin_footer-Ends=".date('Y-m-d H:i:s'));        
}


add_action('load-edit.php', 'isl_woois_custom_bulk_action');
function isl_woois_custom_bulk_action() {
//error_log("isl_woois_custom_bulk_action-Starts=".date('Y-m-d H:i:s'));        
  // 1. get the action
  $wp_list_table = _get_list_table('WP_Posts_List_Table');
  $action = $wp_list_table->current_action();

  // 2. security check
  //check_admin_referer('bulk-posts');

  switch($action) {
    // 3. Perform the action
    case 'potis':
        // make sure ids are submitted.  depending on the resource type, this may be 'media' or 'ids'
    if(isset($_REQUEST['post'])) {
        $post_ids = array_map('intval', $_REQUEST['post']);
        $exportedToIS = 0;
        global $woocommerce;
        $wc_is = new WC_Infusionsoft();
        foreach( $post_ids as $post_id ) {
            $order_id = (int)$post_id;
            $IS_Invoice_IDArr = get_post_meta($order_id,"IS Invoice ID");
            $IS_Invoice_ID = $IS_Invoice_IDArr[0];
            if(strlen($IS_Invoice_ID) <= 0){
                    //Push to Infusionsoft and mark WooOrder as Complete.
                $order = new WC_Order($order_id);
                $wooorderstatus = $order->get_status();
                $wc_is->process_payment($order_id, true, true);
                    //$order->update_status("completed", __( "Push Order To Infusionsoft Has made order Completed", 'woocommerce' ));
                $exportedToIS++;
            }
        }
    }        
        // build the redirect url
    $sendback = add_query_arg( array('post_type' => shop_order), $sendback );
    break;
    
    default: return;
}
//error_log("isl_woois_custom_bulk_action-Ends=".date('Y-m-d H:i:s'));          
  // 4. Redirect client
wp_redirect($sendback);
exit();
}

function isl_woois_order_extra_columns($columns)
{
    $columns['woois_order_id'] = 'IS Invoice Id';
    return $columns;
}
add_filter("manage_edit-shop_order_columns", "isl_woois_order_extra_columns", 99);


function isl_woois_order_extra_columns_content($column)
{
//error_log("isl_woois_order_extra_columns_content-Starts=".date('Y-m-d H:i:s'));                    
    global $post;
    $order_id = $post->ID;
    switch ($column) {
        case "woois_order_id":
        $IS_Invoice_IDArr = get_post_meta($order_id,"IS Invoice ID");
        $IS_Invoice_ID = $IS_Invoice_IDArr[0];
        $is_settings = get_option('woocommerce_infusionsoft_settings');
        $is_order_url = "https://".$is_settings['is_application_name'].".infusionsoft.com/Job/manageJob.jsp?view=edit&ID=".$IS_Invoice_ID;
        echo "<a href=".$is_order_url." target=_blank>".$IS_Invoice_ID."</a>";
        break;
    }
//error_log("isl_woois_order_extra_columns_content-Ends=".date('Y-m-d H:i:s'));                        
}
add_action("manage_posts_custom_column",  "isl_woois_order_extra_columns_content", 99);

?>