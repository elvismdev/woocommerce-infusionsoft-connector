<?php

/**
 * Plugin Name: Woocommerce Infusionsoft Module
 * Plugin URI:  http://www.informationstreet.com/shop/woo-commerce-payment-gateway-infusionsoft/
 * Description: A payment gateway & product importer plugin for Woocommerce for the Infusionsoft system
 * Author:      Information Street
 * Author URI:  http://www.informationstreet.com/
 * Version:     7.6
 *
 *
 * Changelog:
 *
 * 3.0: Now any gateway used to purchase a product will cause the details to end up in IS. Optionally turn off the IS gateway itself by omitting the merchant number. Fixed logging for debug reasons. Included a test gateway, currently commented out.
 * 3.1: Fixed bug whereby tags and goals were fired before payment was complete. Refactored that code down to after the transaction was successful
 * 3.2: Updated debug mode and tested a few things with regard to payments, invoices and affiliates
 * 3.5: No changes... a release package of the same code
 * 3.6: Added Status=1 when adding a new product via the system. Added IonCube Loader check to prevent fatal errors. Fixed bug whereby orders with unknown product IDs caused orders to go through with the wrong values.
 * 3.7: Fixed tax and shipping values below �1 not being added to the order. Also fixed the product import screen which was showing an API error
 * 3.8: Reduced load on the import page by reducing the number of IS results to 500 from 4000. You can still get 4000 by adding high_volume=1 to the URL on the import page as necessary
 * 3.9: Fixed bug whereby you could not set a tag against a product once an original had been set
 * 4.0: Added automatic email optin for contacts created when an order is placed. Also confirmed that the IS module passes tags and applies accordingly (but NOT in test mode)
 * 5.0: Miscellenous fixes
 * 5.1: Licensing and Upgrade Feature added
 * 5.2: Bugs fixed for Add Tags and Update Contacts.
 * 5.3: Bugs fixed for Discount , Total Mismatch for offset by $0.01 and new Feature added for Setting Tax Label, instead of Hardcoded VAT.
 * 5.4: Subscription - Show Offer Details in Product Page and in Checkout.
 * 5.5: add a new setting in the gateway settings - mark woo order as.... When successfully charged - reading in the various statuses woocommerce has and set the setting value.
 * 5.6: Autoupgrade Plugin Code Hooked.
 * 5.7: Added code to populate Card Email.
 * 5.8: WooCommerce Discount Value was wrongly getting calculated, fixed.
 * 5.9: Avoid conflict of Custom Woo Filter Functions.
 * 6.0: SKU for Single and variation products were not pushed to IS.Fixed.
 * 6.1: Fixed Subscription Code for adding Valid CreditCard and Not to immediately charge Invoice in IS.
 * 6.2: Fixed ProductId for NON-VARIATION WOO Products.
 * 6.3: Fixed Line Item Price.
 * 6.4: Added new feature for adding Custom Fileds for Order from Woo Checkout.
 * 6.5: License Code Fix.
 * 6.6: Subscription code fix.
 * 6.7: Added Special Tag to the Contact.
 * 6.8: Email Opt-in, add special tags based on Checkout page Checkbox.
 * 6.9: Added is_sub_length custom field
 * 7.0: Removed Blank Order while creating a Subscription Order.
 * 7.1: Added Order Form URL featire to redirect when add-to-cart is clicked.
 * 7.2: Added Push Order to IS feature in Order Admin Page and Order Custom Field population with ProductsIds of the Order.
 * 7.3: Added Settings Link to the PluginPage.
 * 7.4: Added Affiliation LOgic to support ?affiliate
 * 7.5: Added Weight and Description of Product Import.
 * 7.6: Added IS Product Options(Woo Product Custom Fld is_product_option_ids, 5,17:7,27 format) to be passed to Order._WooProductOptions custom field.
 */
function sb_infusionsoft_init() {
    if (!class_exists('WC_Payment_Gateway')) {
            return;
    }
    
    //if(extension_loaded("IonCube Loader")) {     
        require_once('functions.php');
    //} else {
        //add_action('admin_notices', 'sb_woo_is_no_ioncube');
    //}
}

function sb_woo_is_no_ioncube() {
    echo '<div class="updated fade">
            <p>The Infusionsoft WooCommerce module will not function because you need IonCube Loader to process the file. Once it has been successfully added this message will disappear.</p>
        </div>';
}

add_filter('plugins_loaded', 'sb_infusionsoft_init' );


//custom updates/upgrades
$this_file_woois = __FILE__;
$update_check_woois = "http://www.informationstreet.com/software/isl_plugins_woois.chk";
if(is_admin()){
    require_once('gill-updates-woois.php');
}
?>